﻿using ServerHotel.DAO;
using ServerHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerHotel.BUS
{
    public class Auth
    {
        public static  User checkLogin(string username, string password)
        {
     

            UserDAO userDao = new UserDAO();
            User result  = userDao.IsValidLogin(username, password);

            return result;
        }

        public static User GetInfoUser(string username)
        {

            UserDAO userDao = new UserDAO();
            User retVal = new User();
            retVal = userDao.GetInfoUser(username);

            return retVal;
        }
    }
}