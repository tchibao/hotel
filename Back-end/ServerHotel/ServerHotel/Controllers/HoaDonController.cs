﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ServerHotel.DAO;
using ServerHotel.Models;
using CloudinaryDotNet;
using System.Threading.Tasks;
using System.Web;
using CloudinaryDotNet.Actions;


namespace ServerHotel.Controllers
{
    public class HoaDonController : ApiController
    {
        HoaDonDAO hoaDonDao = new HoaDonDAO();

        [HttpGet]
        [Route("api/hoadon")]
        public HttpResponseMessage GetList(int page = 1)
        {
            PagingLists retVal = new PagingLists();
            retVal = hoaDonDao.getListHoaDon(page);
            if (retVal == null)
            {
                var message = string.Format("Không tìm thấy hóa đơn nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }

        //lay thong tin hoa don
        [HttpGet]
        [Route("api/hoadon/{hoadonID}")]
        public HttpResponseMessage GetHoaDon(string hoadonID)
        {
            HoaDonModel retVal = new HoaDonModel();
            retVal = hoaDonDao.getInfoHoaDon(hoadonID);

            if (retVal == null)
            {
                var message = string.Format("Không tìm thấy hóa đơn này.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            PhongDAO phongDao = new PhongDAO();
            List<string> rooms = phongDao.GetListPhongTheoHoaDon(retVal );

            CheckOutDetailResponse returnObject = new CheckOutDetailResponse
            {
                phongNumber = rooms.ToArray(),
                TotalRoom = rooms.Count,
                totalMoney = retVal.TongTien,
            };

            return Request.CreateResponse(HttpStatusCode.OK, returnObject);
        }
    }
}