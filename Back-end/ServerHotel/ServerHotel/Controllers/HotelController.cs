﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ServerHotel.DAO;
using ServerHotel.Models;
using CloudinaryDotNet;
using System.Threading.Tasks;
using System.Web;
using CloudinaryDotNet.Actions;

namespace ServerHotel.Controllers
{
    public class HotelController : ApiController
    {
        HotelDAO hotelDao = new HotelDAO();
        private Cloudinary _cloudinary;

        public void SetUPCloudinaryService(string apiKey = "998173262265883", string apiSecret = "elfggyRECMbSU8Uihp-ICeZnWtU", string cloudName = "doga4u6vf")
        {
            var myAccount = new Account { ApiKey = apiKey, ApiSecret = apiSecret, Cloud = cloudName };
            _cloudinary = new Cloudinary(myAccount);
        }
        public HotelController()
        {

            SetUPCloudinaryService();

        }

        [HttpPost]
        [Route("api/hotels")]
        public async Task<HttpResponseMessage> CreateNewHotels()
        {
            HotelDAO hotelDao = new HotelDAO();

            // Dictionary<string, object> dict = new Dictionary<string, object>();
            ErrorResponse err = new ErrorResponse();


            try
            {
                string maKS, tenKS, soNha, duong, quan, thanhPho, mota;
                float soSao;
                int giaTB;

                var httpRequest = HttpContext.Current.Request;
                maKS = httpRequest.Form["maKS"];
                if (maKS == null)
                    err.errors.Add("Chưa nhập mã khách sạn");
                if (hotelDao.isHotel(maKS) == false)
                    err.errors.Add("Mã khách sạn đã có");
                tenKS = httpRequest.Form["tenKS"];
                if (tenKS == null)
                    err.errors.Add("Chưa nhập tên khách sạn");
                string soSaoString = httpRequest.Form["soSao"];
                if (soSaoString == null)
                    err.errors.Add("Chưa nhập số sao");
                else
                {
                    if (float.TryParse(soSaoString, out soSao) == false)
                        err.errors.Add("Số sao không hợp lệ");
                }
                duong = httpRequest.Form["duong"];
                if (duong == null)
                    err.errors.Add("Chưa nhập đường");
                soNha = httpRequest.Form["soNha"];
                if (soNha == null)
                    err.errors.Add("Chưa nhập số nhà");
                quan = httpRequest.Form["quan"];
                if (quan == null)
                    err.errors.Add("Chưa nhập quận");
                thanhPho = httpRequest.Form["thanhPho"];
                if (thanhPho == null)
                    err.errors.Add("Chưa nhập Thành phố");
                mota = httpRequest.Form["mota"];
                if (mota == null)
                    err.errors.Add("Chưa nhập Mô tả");
                string giaTBString = httpRequest.Form["giaTB"];
                if (giaTBString == null)
                    err.errors.Add("Chưa nhập giá trung bình");
                else
                {
                    if (int.TryParse(giaTBString, out giaTB) == false)
                        err.errors.Add("Giá Tiền  không hợp lệ");
                }
                var avatar = httpRequest.Files["avatar"];
                if (avatar == null)
                {
                    err.errors.Add("Avatar  không hợp lệ");
                }

                if (err.errors.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, err);

                }

                var uploadParams = new ImageUploadParams
                {
                    File = new FileDescription(httpRequest.Files["avatar"].FileName, httpRequest.Files["avatar"].InputStream),
                    Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
                };

                var uploadResult = _cloudinary.Upload(uploadParams);

                Hotel h = new Hotel()
                {
                    maKS = maKS.Trim(), //DateTime.Now.ToLongTimeString().Substring(0, 9),
                    tenKS = tenKS.Trim(),
                    soSao = float.Parse(soSaoString),
                    soNha = soNha.Trim(),
                    duong = duong.Trim(),
                    quan = quan.Trim(),
                    thanhPho = thanhPho.Trim(),
                    giaTB = int.Parse(giaTBString),
                    moTa = mota,
                    avatar = uploadResult.PublicId

                };

                if (hotelDao.addNewHotel(h) == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, h);

                }
                else
                {
                    DeleteResource(h.avatar);
                    err.errors.Add("Có lỗi xảy ra. Hãy thử lại");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, err);
                }

                //return Request.CreateResponse(HttpStatusCode.NotFound, dict);
                //"/doga4u6vf/image/upload/v1513654570/c5dvmed81gbtpfaffkwr.jpg"
                //
            }
            catch (Exception ex)
            {
                var res = string.Format("some Message");
                err.errors.Add(res);
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        [Route("api/hotels")]
        public async Task<HttpResponseMessage> UpdateHotels()
        {
            HotelDAO hotelDao = new HotelDAO();

            // Dictionary<string, object> dict = new Dictionary<string, object>();
            ErrorResponse err = new ErrorResponse();

            try
            {
                string maKS, tenKS, soNha, duong, quan, thanhPho, mota;
                float soSao;
                int giaTB;

                var httpRequest = HttpContext.Current.Request;
                maKS = httpRequest.Form["maKS"];
                tenKS = httpRequest.Form["tenKS"];
                if (tenKS == null)
                    err.errors.Add("Chưa nhập tên khách sạn");
                string soSaoString = httpRequest.Form["soSao"];
                if (soSaoString == null)
                    err.errors.Add("Chưa nhập số sao");
                else
                {
                    if (float.TryParse(soSaoString, out soSao) == false)
                        err.errors.Add("Số sao không hợp lệ");
                }
                duong = httpRequest.Form["duong"];
                if (duong == null)
                    err.errors.Add("Chưa nhập đường");
                soNha = httpRequest.Form["soNha"];
                if (soNha == null)
                    err.errors.Add("Chưa nhập số nhà");
                quan = httpRequest.Form["quan"];
                if (quan == null)
                    err.errors.Add("Chưa nhập quận");
                thanhPho = httpRequest.Form["thanhPho"];
                if (thanhPho == null)
                    err.errors.Add("Chưa nhập Thành phố");
                mota = httpRequest.Form["mota"];
                if (mota == null)
                    err.errors.Add("Chưa nhập Mô tả");
                string giaTBString = httpRequest.Form["giaTB"];
                if (giaTBString == null)
                    err.errors.Add("Chưa nhập giá trung bình");
                else
                {
                    if (int.TryParse(giaTBString, out giaTB) == false)
                        err.errors.Add("Giá tiền không hợp lệ");
                }
                var avatar = httpRequest.Files["avatar"];
                if (avatar == null)
                {
                    err.errors.Add("Avatar  không hợp lệ");
                }

                if (err.errors.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, err);

                }
                //Get old Hotel
                Hotel hotel = hotelDao.getHotel(maKS);
                string oldImage = hotel.avatarID.Trim();
                //DeleteResource(hotel.avatar);
                var uploadParams = new ImageUploadParams
                {
                    File = new FileDescription(httpRequest.Files["avatar"].FileName, httpRequest.Files["avatar"].InputStream),
                    Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
                };

                var uploadResult = _cloudinary.Upload(uploadParams);

                Hotel h = new Hotel()
                {
                    maKS = maKS.Trim(), //DateTime.Now.ToLongTimeString().Substring(0, 9),
                    tenKS = tenKS.Trim(),
                    soSao = float.Parse(soSaoString),
                    soNha = soNha.Trim(),
                    duong = duong.Trim(),
                    quan = quan.Trim(),
                    thanhPho = thanhPho.Trim(),
                    giaTB = int.Parse(giaTBString),
                    moTa = mota,
                    avatar = uploadResult.PublicId

                };

                if (hotelDao.updateHotel(h) == true)
                {
                    DeleteResource(oldImage);

                    return Request.CreateResponse(HttpStatusCode.OK, h);

                }
                else
                {
                    DeleteResource(h.avatar);
                    err.errors.Add("Có lỗi xảy ra. Hãy thử lại");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, err);
                }

            }
            catch (Exception ex)
            {
                var res = string.Format("some Message");
                err.errors.Add(res);
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [Authorize]
        [HttpDelete]
        [Route("api/hotels/{hotelID}")]
        public HttpResponseMessage DeleteList(string hotelID)
        {
            ErrorResponse err = new ErrorResponse();
            var hotel = hotelDao.getHotel(hotelID);
            if(hotel == null)
            {
                err.errors.Add("Hotel khong ton tai");
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            if (!hotelDao.checkCanDelete(hotelID))
            {
                err.errors.Add("Không thể xóa hotel này");
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, err);
            }
             

            //deleteImage
            if (hotelDao.deleteHotel(hotelID) == true)
            {
                DeleteResource(hotel.avatarID);
                return Request.CreateResponse(HttpStatusCode.OK, hotelID);
            }
            err.errors.Add("Xóa thất bại. Hãy thử lại.");
            return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            
        }

        //Delete IMG
        private void DeleteResource(string publicID)
        {
            var delParams = new DelResParams()
            {
                PublicIds = new List<string>() { publicID },
                Invalidate = true
            };

            _cloudinary.DeleteResources(delParams);
        }


        //Create new hotel

        //Lấy danh sách khách sạn
        [HttpGet]
        [Route("api/hotel")]
        public HttpResponseMessage GetList(int page =1)
        {
            HotelPaging retVal = new HotelPaging();
            retVal = hotelDao.getListHotel(page);
            if (retVal.hotel == null)
            {
                var message = string.Format("Không tìm thấy khách sạn nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }

        //Lấy thông tin của khách sạn
        [HttpGet]
        [Route("api/hotel/{hotelID}")]
        public HttpResponseMessage GetHotel(string hotelID)
        {
            Hotel retVal = new Hotel();
            retVal = hotelDao.getHotel(hotelID);

            if (retVal == null)
            {
                var message = string.Format("Không tìm thấy khách sạn này.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }

            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }

        ////Thêm khách sạn
        //[HttpPost]
        //[Route("api/hotel/add")]
        //public HttpResponseMessage AddHotel(string id)
        //{
        //    Hotel retVal = new Hotel();
        //    retVal = hotelDao.getHotel(id);

        //    if (retVal == null)
        //    {
        //        var message = string.Format("Hotel not found");
        //        HttpError err = new HttpError(message);
        //        return Request.CreateResponse(HttpStatusCode.NotFound, err);
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, retVal);
        //}

        //Tìm kiếm khách sạn theo nhiều tiêu chí
        [HttpGet]
        [Route("api/findhotel")]
        public HttpResponseMessage FindHotel(int soTien2 ,int soTien1= -1, int soSao1 = 0, int soSao2 = -1, string thanhPho = "", int page = 1)
        {
            HotelPaging retVal = new HotelPaging();

            if (thanhPho == null)
            {
                thanhPho = "";
            }
            retVal = hotelDao.FindListHotel(soTien1, soTien2, soSao1, soSao2, thanhPho, page);

            if (retVal.hotel == null)
            {
                var message = string.Format("Không tìm thấy khách sạn nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }

            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }

        [HttpGet]
        [Route("api/findHotel/admin")]
        public HttpResponseMessage FindHotelAdmin(string query = "", int page = 1)
        {
            HotelPaging retVal = new HotelPaging();

            if (query == null)
            {
                query = "";
            }
            retVal = hotelDao.FindListHotelAdmin(query, page);

            if (retVal.hotel == null)
            {
                var message = string.Format("Không tìm thấy khách sạn nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }

            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }
        //tìm kiếm khách sạn theo thành phố
        [HttpGet]
        [Route("api/find")]
        public HttpResponseMessage FindHotel(string thanhPho, int page=1)
        {
            HotelPaging retVal = new HotelPaging();
            if(thanhPho == null)
            {
                thanhPho = "";
            }
            thanhPho = thanhPho.Replace('-', ' ');

            retVal = hotelDao.FindListHotel(thanhPho, page);

            if (retVal.hotel == null)
            {
                var message = string.Format("Không tìm thấy khách sạn nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }

            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }



    }
}
