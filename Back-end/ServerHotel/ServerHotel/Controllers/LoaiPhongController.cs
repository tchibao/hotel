﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using ServerHotel.DAO;
using ServerHotel.Models;
using ServerHotel.Ults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ServerHotel.Controllers
{
    public class LoaiPhongController : ApiController
    {
        LoaiPhongDAO loaiphongDao = new LoaiPhongDAO();
        [HttpGet]
        [Route("api/hotel/{hotelID}/types")]
        public HttpResponseMessage GetList(string hotelID,int page = 1)
        {
            List<LoaiPhong> retVal = new List<LoaiPhong>();
            retVal = loaiphongDao.getList(hotelID, page);
            if (retVal == null)
            {
                var message = string.Format("Không tìm thấy loại phòng nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }

        [HttpGet]
        [Route("api/loaiphong/{loaiPhongID}")]
        public HttpResponseMessage GetItem(string loaiPhongID)
        {
            LoaiPhong retVal = new LoaiPhong();
            retVal = loaiphongDao.getDetail(loaiPhongID);
            if (retVal == null)
            {
                var message = string.Format("Không tìm thấy khách sạn nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }

            //config imgLink
            retVal.testImg = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", retVal.testImg);
            retVal.detailImage1 = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", retVal.detailImage1);
            retVal.detailImage2 = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", retVal.detailImage2);
            retVal.detailImage3 = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", retVal.detailImage3);
            retVal.detailImage4 = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", retVal.detailImage4);

            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }

        [HttpDelete]
        [Route("api/roomTypes/{roomtypeID}")]
        public HttpResponseMessage DeleteRoomType(string roomtypeID)
        {
            ErrorResponse err = new ErrorResponse();
            var roomType = loaiphongDao.getDetail(roomtypeID);
            if (roomType == null)
            {
                err.errors.Add("Hotel khong ton tai");
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, err);
            }


            if (!loaiphongDao.checkCanDelete(roomtypeID))
            {
                err.errors.Add("Không thể xóa loại phòng  này");
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, err);
            }
            //deleteImage
            if (loaiphongDao.deleteRoomType(roomtypeID) == true)
            {
                //CloudiaryServices.Singleton.DeleteResource(roomType.testImg);
                //CloudiaryServices.Singleton.DeleteResource(roomType.detailImage1);
                //CloudiaryServices.Singleton.DeleteResource(roomType.detailImage2);
                //CloudiaryServices.Singleton.DeleteResource(roomType.detailImage3);
                //CloudiaryServices.Singleton.DeleteResource(roomType.detailImage4);


                return Request.CreateResponse(HttpStatusCode.OK, roomType);
            }
            err.errors.Add("Xóa thất bại. Hãy thử lại.");
            return Request.CreateResponse(HttpStatusCode.BadRequest, err);

        }


        [HttpPut]
        [Route("api/roomType")]
        public async Task<HttpResponseMessage> UpdateHotels()
        {
            ErrorResponse err = new ErrorResponse();


            try
            {
                string maLoaiPhong, tenLoaiPhong, moTa, maKS;
                //string maKS, tenKS, soNha, duong, quan, thanhPho, mota;
                //float soSao;
                int donGia = 0;

                var httpRequest = HttpContext.Current.Request;
                maLoaiPhong = httpRequest.Form["maLoaiPhong"];
                LoaiPhong cached = loaiphongDao.getDetail(maLoaiPhong);

                if (cached == null)
                    err.errors.Add("Loại phòng không tồn tại");


                tenLoaiPhong = httpRequest.Form["tenLoaiPhong"];
                if (tenLoaiPhong == null)
                    err.errors.Add("Chưa nhập tên loại phòng");



                moTa = httpRequest.Form["moTa"];
                if (moTa == null)
                    err.errors.Add("Chưa nhập Mô tả");
                string donGiaString = httpRequest.Form["donGia"];
                if (donGiaString == null)
                    err.errors.Add("Chưa nhập đơn giá");
                else
                {
                    if (int.TryParse(donGiaString, out donGia) == false)
                        err.errors.Add("Giá Tiền  không hợp lệ");
                }


                if (err.errors.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, err);

                }


                //Update loai phong
                cached.tenLoaiPhong = tenLoaiPhong;
                cached.moTa = moTa;
                cached.donGia = donGia;
                var avatar = httpRequest.Files["avatar"];
                //List string to change
                List<string> id = new List<string>();
                if (avatar != null)
                {
                    var uploadParams = new ImageUploadParams
                    {
                        File = new FileDescription(avatar.FileName, avatar.InputStream),
                        Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
                    };
                    var uploadResult = await CloudiaryServices.Singleton.UploadImage(uploadParams);
                    id.Add(cached.testImg);
                    cached.testImg = uploadResult.PublicId;

                }
                var detail1 = httpRequest.Files["detail1"];
                if (detail1 != null)
                {
                    var detail1Params = new ImageUploadParams
                    {
                        File = new FileDescription(detail1.FileName, detail1.InputStream),
                        Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
                    };
                    var detail1Result = await CloudiaryServices.Singleton.UploadImage(detail1Params);

                    id.Add(cached.detailImage1);
                    cached.detailImage1 = detail1Result.PublicId;

                }
                var detail2 = httpRequest.Files["detail2"];
                if (detail2 != null)
                {
                    var detail2Params = new ImageUploadParams
                    {
                        File = new FileDescription(detail2.FileName, detail2.InputStream),
                        Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
                    };
                    var detail2Result = await CloudiaryServices.Singleton.UploadImage(detail2Params);


                    id.Add(cached.detailImage2);
                    cached.detailImage2 = detail2Result.PublicId;
                }
                var detail3 = httpRequest.Files["detail3"];
                if (detail3 != null)
                {
                    var detail3Params = new ImageUploadParams
                    {
                        File = new FileDescription(detail3.FileName, detail3.InputStream),
                        Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
                    };
                    var detail3Result = await CloudiaryServices.Singleton.UploadImage(detail3Params);

                    id.Add(cached.detailImage3);
                    cached.detailImage3 = detail3Result.PublicId;
                }
                var detail4 = httpRequest.Files["detail4"];
                if (detail4 != null)
                {
                    var detail4Params = new ImageUploadParams
                    {
                        File = new FileDescription(detail4.FileName, detail4.InputStream),
                        Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
                    };
                    var detail4Result = await CloudiaryServices.Singleton.UploadImage(detail4Params);

                    id.Add(cached.detailImage4);
                    cached.detailImage4 = detail4Result.PublicId;
                }







                






                if (loaiphongDao.updateLoaiPhong(cached) == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, cached);

                }
                else
                {
                    foreach(string idToRemove in id)
                    {
                        CloudiaryServices.Singleton.DeleteResource(idToRemove);

                    }



                    err.errors.Add("Có lỗi xảy ra. Hãy thử lại");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, err);
                }

                //return Request.CreateResponse(HttpStatusCode.NotFound, dict);
                //"/doga4u6vf/image/upload/v1513654570/c5dvmed81gbtpfaffkwr.jpg"
                //
            }
            catch (Exception ex)
            {
                var res = string.Format("some Message");
                err.errors.Add(res);
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("api/roomType")]
        public async Task<HttpResponseMessage> CreateRoomType()
        {
            //HotelDAO hotelDao = new HotelDAO();

            // Dictionary<string, object> dict = new Dictionary<string, object>();
            ErrorResponse err = new ErrorResponse();


            try
            {
                string maLoaiPhong, tenLoaiPhong, moTa,maKS;
                //string maKS, tenKS, soNha, duong, quan, thanhPho, mota;
                //float soSao;
                int donGia;

                var httpRequest = HttpContext.Current.Request;
                maLoaiPhong = httpRequest.Form["maLoaiPhong"];
                if (maLoaiPhong == null)
                    err.errors.Add("Chưa nhập mã phòng");

                if (maLoaiPhong != null && maLoaiPhong.Length > 10)
                    err.errors.Add("Mã loại phòng chỉ có 10 sô");
                if (loaiphongDao.isRoomType(maLoaiPhong) == false)
                    err.errors.Add("Mã loại phòng sạn đã có");

                maKS = httpRequest.Form["maKS"];
                if (maKS == null)
                    err.errors.Add("Chưa nhập mã khách sạn");


                tenLoaiPhong = httpRequest.Form["tenLoaiPhong"];
                if (tenLoaiPhong == null)
                    err.errors.Add("Chưa nhập tên loại phòng");



                moTa = httpRequest.Form["moTa"];
                if (moTa == null)
                    err.errors.Add("Chưa nhập Mô tả");
                string donGiaString = httpRequest.Form["donGia"];
                if (donGiaString == null)
                    err.errors.Add("Chưa nhập đơn giá");
                else
                {
                    if (int.TryParse(donGiaString, out donGia) == false)
                        err.errors.Add("Giá Tiền  không hợp lệ");
                }
                var avatar = httpRequest.Files["avatar"];
                if (avatar == null)
                {
                    err.errors.Add("Avatar  không hợp lệ");
                }
                var detail1 = httpRequest.Files["detail1"];
                if (detail1 == null)
                {
                    err.errors.Add("Detail image 1  không hợp lệ");
                }
                var detail2 = httpRequest.Files["detail2"];
                if (detail2 == null)
                {
                    err.errors.Add("Detail image 2  không hợp lệ");
                }
                var detail3 = httpRequest.Files["detail3"];
                if (detail3 == null)
                {
                    err.errors.Add("Detail image 3  không hợp lệ");
                }
                var detail4 = httpRequest.Files["detail4"];
                if (detail4 == null)
                {
                    err.errors.Add("Detail image 4  không hợp lệ");
                }

                if (err.errors.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, err);

                }


                var uploadParams = new ImageUploadParams
                {
                    File = new FileDescription(avatar.FileName,avatar.InputStream),
                    Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
                };


                var detail1Params = new ImageUploadParams
                {
                    File = new FileDescription(detail1.FileName, detail1.InputStream),
                    Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
                };

                var detail2Params = new ImageUploadParams
                {
                    File = new FileDescription(detail2.FileName, detail2.InputStream),
                    Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
                };

                var detail3Params = new ImageUploadParams
                {
                    File = new FileDescription(detail3.FileName, detail3.InputStream),
                    Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
                };

                var detail4Params = new ImageUploadParams
                {
                    File = new FileDescription(detail4.FileName, detail4.InputStream),
                    Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
                };

                var uploadResult =   CloudiaryServices.Singleton.UploadImage(uploadParams);
                var detail1Result =  CloudiaryServices.Singleton.UploadImage(detail1Params);
                var detail2Result =  CloudiaryServices.Singleton.UploadImage(detail2Params);
                var detail3Result =  CloudiaryServices.Singleton.UploadImage(detail3Params);
                var detail4Result =  CloudiaryServices.Singleton.UploadImage(detail4Params);

                await Task.WhenAll(uploadResult, detail1Result, detail2Result, detail3Result,detail4Result);



                LoaiPhong h = new LoaiPhong()
                {
                    maLoaiPhong = maLoaiPhong.Trim(), //DateTime.Now.ToLongTimeString().Substring(0, 9),
                    tenLoaiPhong = tenLoaiPhong.Trim(),
      
                    maKS = maKS.Trim(),
                    donGia = int.Parse(donGiaString),
                    moTa = moTa,
                    slTrong = 0,

                    testImg = uploadResult.Result.PublicId,
                    detailImage1 = detail1Result.Result.PublicId,
                    detailImage2 = detail2Result.Result.PublicId,
                    detailImage3 = detail3Result.Result.PublicId,
                    detailImage4 = detail4Result.Result.PublicId,



                };

                if (loaiphongDao.addRoomType(h) == true)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, h);

                }
                else
                {
                    CloudiaryServices.Singleton.DeleteResource(h.testImg);
                    CloudiaryServices.Singleton.DeleteResource(h.detailImage1);
                    CloudiaryServices.Singleton.DeleteResource(h.detailImage2);
                    CloudiaryServices.Singleton.DeleteResource(h.detailImage3);
                    CloudiaryServices.Singleton.DeleteResource(h.detailImage4);


                    err.errors.Add("Có lỗi xảy ra. Hãy thử lại");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, err);
                }

                //return Request.CreateResponse(HttpStatusCode.NotFound, dict);
                //"/doga4u6vf/image/upload/v1513654570/c5dvmed81gbtpfaffkwr.jpg"
                //
            }
            catch (Exception ex)
            {
                var res = string.Format("some Message");
                err.errors.Add(res);
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
