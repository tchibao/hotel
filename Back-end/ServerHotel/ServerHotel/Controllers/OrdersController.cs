﻿using ServerHotel.DAO;
using ServerHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace ServerHotel.Controllers
{
    public class OrdersController : ApiController
    {
        DatPhongDAO datphongDao = new DatPhongDAO();
        //[Authorize(Roles = "1")]
        //[HttpGet]f
        //[Route("api/orders")]
        //public HttpResponseMessage BookRoom()
        //{
        //    ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;

        //    return Request.CreateResponse(HttpStatusCode.OK);
        //}


       // [Authorize]
        [HttpPost]
        [Route("api/orders")]
        public HttpResponseMessage BookRoom([FromBody] DatPhongModel datPhong)
        {
            //ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;
            if (ModelState.IsValid)
            {

                datPhong.maDB = datphongDao.getMaDP();
                datPhong.ngayDat = DateTime.Now;

                if (datphongDao.DatPhong(datPhong))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, datPhong);

                }
                ErrorResponse err = new ErrorResponse();
                err.errors.Add("Đặt phòng không thành công");
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, err);

            }

            IEnumerable<string> modelStateErrors =
                from state in ModelState.Values
                from error in state.Errors
                select error.ErrorMessage;

            ErrorResponse e = new ErrorResponse();
            e.errors = modelStateErrors.ToList();


            return Request.CreateResponse(HttpStatusCode.NotAcceptable, e);

        }


        [HttpGet]
        [Route("api/orders/unConfirm")]
        public HttpResponseMessage GetUnCofirmList(int page = 1)
        {
            ErrorResponse err = new ErrorResponse();
            var datPhong = datphongDao.getUnconfirmList(page);

            if (datPhong == null)
            {
                err.errors.Add("Không tìm thấy");
                return Request.CreateResponse(HttpStatusCode.NoContent, err);
            }


            return Request.CreateResponse(HttpStatusCode.OK, datPhong);
        }

        [HttpGet]
        [Route("api/orders/checkedIn")]
        public HttpResponseMessage getCheckedIn(int page = 1)
        {
            ErrorResponse err = new ErrorResponse();
            var datPhong = datphongDao.getCheckedIn(page);

            if (datPhong == null)
            {
                err.errors.Add("Không tìm thấy");
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }


            return Request.CreateResponse(HttpStatusCode.OK, datPhong);
        }

        [HttpGet]
        [Route("api/orders/confirm")]
        public HttpResponseMessage GetCofirmList(int page = 1)
        {
            ErrorResponse err = new ErrorResponse();
            var datPhong = datphongDao.getConfirmList(page);

            if (datPhong == null)
            {
                err.errors.Add("Không tìm thấy");
                return Request.CreateResponse(HttpStatusCode.NoContent, err);
            }


            return Request.CreateResponse(HttpStatusCode.OK, datPhong);
        }

        [HttpPost]
        [Route("api/orders/confirm")]
        public HttpResponseMessage ConfirmOrder([FromBody] DatPhongModel datPhong)
        {
            ErrorResponse err = new ErrorResponse();
            if(datPhong.maDB == null){
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            if (datphongDao.confirmOrder(datPhong.maDB))
            {
                return Request.CreateResponse(HttpStatusCode.OK);

            }
            return Request.CreateResponse(HttpStatusCode.NoContent);
    


        }

        [HttpPost]
        [Route("api/orders/cancel")]
        public HttpResponseMessage CancelOrder([FromBody] DatPhongModel datPhong)
        {
            ErrorResponse err = new ErrorResponse();
            if (datPhong.maDB == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            if (datphongDao.cancelOrder(datPhong.maDB))
            {
                return Request.CreateResponse(HttpStatusCode.OK);

            }
            return Request.CreateResponse(HttpStatusCode.NoContent);

        }

        [HttpGet]
        [Route("api/orders/list")]
        public HttpResponseMessage GetListDatPhong(int page = 1)
        {
            ErrorResponse err = new ErrorResponse();
            var datPhong = datphongDao.getListDatPhong(page);

            if (datPhong == null)
            {
                err.errors.Add("Không có phiếu đặt phòng nào");
                return Request.CreateResponse(HttpStatusCode.NoContent, err);
            }


            return Request.CreateResponse(HttpStatusCode.OK, datPhong);
        }
    }
}
