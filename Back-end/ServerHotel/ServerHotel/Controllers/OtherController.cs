﻿using ServerHotel.DAO;
using ServerHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServerHotel.Controllers
{
    public class OtherController : ApiController
    {
        OtherDAO otherDao = new OtherDAO();

        [HttpGet]
        [Route("api/thanhPho")]
        public HttpResponseMessage GetListThanhPho()
        {
            List<ThanhPho> retVal = new List<ThanhPho>();
            retVal = otherDao.getThanhPho();
            if (retVal == null)
            {
                var message = string.Format("Không tìm thấy thành phố nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            return Request.CreateResponse(HttpStatusCode.OK, retVal);


        }


        [HttpGet]
        [Route("api/thanhPho/{thanhPhoID}")]
        public HttpResponseMessage GetListQuan(string thanhPhoID)
        {
            List<Quan> retVal = new List<Quan>();
            retVal = otherDao.getQuan(thanhPhoID);
            if (retVal == null)
            {
                var message = string.Format("Không tìm thấy quận nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }
    }
}
