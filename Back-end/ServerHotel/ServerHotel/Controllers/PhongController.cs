﻿using ServerHotel.DAO;
using ServerHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServerHotel.Controllers
{
    public class PhongController : ApiController
    {
        PhongDAO phongDao = new PhongDAO();

        [HttpGet]
        [Route("api/phong/{maKS}")]
        public HttpResponseMessage GetList(string maKS,int page =1 )
        {
            PagingLists retVal = null;
            retVal = phongDao.getList(page,maKS);
            if (retVal == null)
            {
                var message = string.Format("Không tìm thấy phòng nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }

            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }

        [HttpPost]
        [Route("api/checkin")]
        public HttpResponseMessage GetListPhongChuaDatTheoLoaiPhong([FromBody] ListPhongTest test)
        {

            ErrorResponse e = new ErrorResponse();
            if (test.lstPhong == null || test.lstPhong.Length < 1)
                e.errors.Add("Phòng không hợp lệ");
            if (test.maDP == null || test.maDP == "")
                e.errors.Add("Mã không hợp lệ");

            if(e.errors.Count > 0)
                return Request.CreateResponse(HttpStatusCode.NoContent, e);


            if (phongDao.CheckIn(test))
            {
                return Request.CreateResponse(HttpStatusCode.OK, test);

            }

            e.errors.Add("Có lỗi xảy ra, xui lòng thửu lại sau");

            return Request.CreateResponse(HttpStatusCode.NoContent, e);

        }
        [HttpPost]
        [Route("api/checkOut")]
        public HttpResponseMessage CheckOutRoom([FromBody] CheckOutPostModel model)
        {

            ErrorResponse e = new ErrorResponse();
            if (model.lstPhong == null || model.lstPhong.Length < 1)
                e.errors.Add("Phòng không hợp lệ");
            if (model.maDP == null || model.maDP.Length < 1)
                e.errors.Add("Mã đặt phòng không hợp lệ");
            if (model.tongTien <= 0 )
                e.errors.Add("Tiền không hợp lệ");


            if (e.errors.Count > 0)
                return Request.CreateResponse(HttpStatusCode.NoContent, e);

            HoaDonModel retVal = phongDao.CheckOut(model);
            if (retVal != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, retVal);

            }

            e.errors.Add("Có lỗi xảy ra, xui lòng thửu lại sau");

            return Request.CreateResponse(HttpStatusCode.NoContent, e);

        }

        [HttpPost]
        [Route("api/checkout/detail")]
        public HttpResponseMessage GetListPhongForCheckOut([FromBody] DatPhongModel model)
        {

            ErrorResponse e = new ErrorResponse();
            if (model.maDB == null || model.maDB == "" )
                e.errors.Add("Đặt phòng không hợp lệ");
            DateTime? bookDate;
            List<string> rooms = phongDao.GetListPhong(model,out bookDate);
            if(rooms.Count < 1)
            {
                e.errors.Add("Đặt phòng không hợp lệ");

            }
            if (e.errors.Count > 0)
                return Request.CreateResponse(HttpStatusCode.NoContent, e);

            int date = (int)DateTime.Today.Subtract(bookDate.Value).TotalDays;
            int TotalMoney = model.DonGia * date;

            CheckOutDetailResponse returnObject = new CheckOutDetailResponse
            {
                phongNumber = rooms.ToArray(),
                totalMoney = TotalMoney,
                TotalRoom = rooms.Count
            };

            return Request.CreateResponse(HttpStatusCode.OK, returnObject);
        }

        [HttpGet]
        [Route("api/{idLoaiPhong}/phongChuaDung")]
        public HttpResponseMessage GetListPhongChuaDatTheoLoaiPhong(string idLoaiPhong)
        {
            List<Phong> retVal = null;
            retVal = phongDao.GetListPhongChuaDatTheoLoaiPhong(idLoaiPhong);
            if (retVal == null)
            {
                var message = string.Format("Không tìm thấy phòng nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }


        [HttpPost]
        [Route("api/phong")]
        public  HttpResponseMessage CreateNewPhong(Phong phong)
        {

            if (ModelState.IsValid)
            {
                if (phongDao.addRoom(phong))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, phong);

                }
                else
                {
                    ErrorResponse e = new ErrorResponse();
                    e.errors.Add("Có lỗi xảy ra vui lòng thử lại sau!");
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, e);
                }
               
            }
            else
            {
                //Convert to message
                IEnumerable<string> modelStateErrors =
                               from state in ModelState.Values
                               from error in state.Errors
                               select error.ErrorMessage;

                ErrorResponse e = new ErrorResponse();
                e.errors = modelStateErrors.ToList();


                return Request.CreateResponse(HttpStatusCode.NotAcceptable, e);
            }

        }

        ////get list phong theo hoa don
        //[HttpPost]
        //[Route("api/hoadon/detail/rooms")]
        //public HttpResponseMessage GetListPhongForHoaDon([FromBody] HoaDonModel model)
        //{

        //    ErrorResponse e = new ErrorResponse();
        //    if (model.maDP == null || model.maDP == "")
        //        e.errors.Add("Đặt phòng không hợp lệ");

        //    List<string> rooms = phongDao.GetListPhongTheoHoaDon(model);
        //    if (rooms.Count < 1)
        //    {
        //        e.errors.Add("Đặt phòng không hợp lệ");

        //    }
        //    if (e.errors.Count > 0)
        //        return Request.CreateResponse(HttpStatusCode.NoContent, e);

        //    CheckOutDetailResponse returnObject = new CheckOutDetailResponse
        //    {
        //        phongNumber = rooms.ToArray(),
        //        TotalRoom = rooms.Count,
        //        totalMoney = model.TongTien,
        //    };

        //    return Request.CreateResponse(HttpStatusCode.OK, returnObject);
        //}
    }
}
