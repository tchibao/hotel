﻿using ServerHotel.DAO;
using ServerHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServerHotel.Controllers
{
    public class ReportController : ApiController
    {
        ReportDAO reportDao = new ReportDAO();

        [HttpGet]
        [Route("api/report/income")]
        public HttpResponseMessage GetList(int? year)
        {
            if(year == null)
            {
                var message = string.Format("Năm không hợp lệ .");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            List<Report> retVal = new List<Report>();
            retVal = reportDao.getReportByYear(year.Value);

            if (retVal == null)
            {
                var message = string.Format("Không tìm thấy hóa đơn nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            return Request.CreateResponse(HttpStatusCode.OK, retVal);
        }


        [HttpGet]
        [Route("api/report/income")]
        public HttpResponseMessage GetList(string DateString)
        {
            //if (year == null)
            //{
            //    var message = string.Format("Năm không hợp lệ .");
            //    HttpError err = new HttpError(message);
            //    return Request.CreateResponse(HttpStatusCode.NotFound, err);
            //}
            try
            {
                string[] parse = DateString.Split('/');
                int month = int.Parse(parse[0]);
                int year = int.Parse(parse[1]);

                DateTime beginofMonth = new DateTime(year, month, 1);
                DateTime endOfMonth = beginofMonth.AddMonths(1);
                endOfMonth =  endOfMonth.AddDays(-1);
                List<Report> retVal = new List<Report>();
                retVal = reportDao.getReportByDate(beginofMonth, endOfMonth);

                if (retVal == null)
                {
                    var message = string.Format("Không tìm thấy hóa đơn nào.");
                    HttpError err = new HttpError(message);
                    return Request.CreateResponse(HttpStatusCode.NotFound, err);
                }
                return Request.CreateResponse(HttpStatusCode.OK, retVal);
            }                
            catch
            {
                var message = string.Format("Hãy chọn tháng để xem.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            
        }
    }
}
