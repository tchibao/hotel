﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ServerHotel.DAO;
using ServerHotel.Models;
using ServerHotel.Ults;
using System.Threading.Tasks;
using System.Web;
using System.Diagnostics;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;

namespace ServerHotel.Controllers
{
    public class UserController : ApiController
    {
        private  Cloudinary _cloudinary;

        public  void SetUPCloudinaryService(string apiKey = "998173262265883", string apiSecret = "elfggyRECMbSU8Uihp-ICeZnWtU", string cloudName = "doga4u6vf")
        {
            var myAccount = new Account { ApiKey = apiKey, ApiSecret = apiSecret, Cloud = cloudName };
            _cloudinary = new Cloudinary(myAccount);
        }
        public UserController()
        {
            SetUPCloudinaryService();
        }
        UserDAO userDao = new UserDAO();

        //[HttpPost]
        //[Route("api/user/test")]
        //public async Task<HttpResponseMessage> CreateNewHotel()
        //{
        //    // Dictionary<string, object> dict = new Dictionary<string, object>();
        //    ErrorResponse err = new ErrorResponse();
        //    try
        //    {

        //        var httpRequest = HttpContext.Current.Request;

        //        foreach (string file in httpRequest.Files)
        //        {
        //            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

        //            var postedFile = httpRequest.Files[file];
        //            if (postedFile != null && postedFile.ContentLength > 0)
        //            {

        //                int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB  

        //                IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
        //                var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
        //                var extension = ext.ToLower();
        //                if (!AllowedFileExtensions.Contains(extension))
        //                {

        //                    var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

        //                    err.errors.Add(message);
        //                    return Request.CreateResponse(HttpStatusCode.BadRequest, err);
        //                }
        //                else if (postedFile.ContentLength > MaxContentLength)
        //                {

        //                    var message = string.Format("Please Upload a file upto 1 mb.");

        //                    err.errors.Add(message);

        //                    return Request.CreateResponse(HttpStatusCode.BadRequest, err);
        //                }
        //                else
        //                {



        //                    var filePath = HttpContext.Current.Server.MapPath("~/Userimage/" + postedFile.FileName + extension);

        //                    postedFile.SaveAs(filePath);

        //                }
        //            }

        //            var message1 = string.Format("Image Updated Successfully.");
        //            return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
        //        }
        //        var res = string.Format("Please Upload a image.");
        //        err.errors.Add(res);
        //        var uploadParams = new ImageUploadParams
        //        {
        //            File = new FileDescription(httpRequest.Files["images"].FileName, httpRequest.Files["images"].InputStream),
        //            Transformation = new Transformation().Width(200).Height(200).Crop("thumb").Gravity("face")
        //        };

        //        var uploadResult = _cloudinary.Upload(uploadParams);

        //        //return Request.CreateResponse(HttpStatusCode.NotFound, dict);
        //        //"/doga4u6vf/image/upload/v1513654570/c5dvmed81gbtpfaffkwr.jpg"
        //        //
        //        return Request.CreateResponse(HttpStatusCode.OK, uploadResult.PublicId);
        //    }
        //    catch (Exception ex)
        //    {
        //        var res = string.Format("some Message");
        //        err.errors.Add(res);
        //        return Request.CreateResponse(HttpStatusCode.NotFound, err);
        //    }
        //}


        [HttpGet]
        [Route("api/user")]
        public HttpResponseMessage GetLists(int page = 1)
        {
            var retVal = userDao.getListUser(page);
            if (retVal.objects == null || retVal.objects.Count < 1)
            {
                var message = string.Format("Không tìm thấy user nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            return Request.CreateResponse(HttpStatusCode.OK, retVal);

        }
        [HttpGet]
        [Route("api/user/{id}")]
        public HttpResponseMessage getDetail(string id)
        {
            var retVal = userDao.getDetail(id);
            if (retVal == null )
            {
                var message = string.Format("Không tìm thấy user nào.");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }

            return Request.CreateResponse(HttpStatusCode.OK, retVal);

        }
        [HttpPost]
        [Route("api/user")]
        public HttpResponseMessage SignUp( User user)
        {
            if (ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.OK,user);
            }
            else
            {
                //Convert to message
                IEnumerable<string> modelStateErrors =
                               from state in ModelState.Values
                               from error in state.Errors
                               select error.ErrorMessage;

                ErrorResponse e = new ErrorResponse();
                e.errors = modelStateErrors.ToList();
                

                return Request.CreateResponse(HttpStatusCode.NotAcceptable, e);
            }
        }

        [HttpPost]
        [Route("api/user/create")]
        public HttpResponseMessage CreateNew(User user)
        {
            if (ModelState.IsValid)
            {
                if (user.tenDangNhap.Contains(" "))
                {
                    ErrorResponse e = new ErrorResponse();
                    e.errors.Add("Tên đăng nhập chứa ");
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, e);
                }
                user.matKhau = Helper.Encrypt("123456");
                if( userDao.addUser(user))
                    return Request.CreateResponse(HttpStatusCode.OK, user);
                else
                {
                    ErrorResponse e = new ErrorResponse();
                    e.errors.Add("Có lỗi xảy ra vui lòng thử lại sau!");
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, e);

                }
            }
            else
            {
                //Convert to message
                IEnumerable<string> modelStateErrors =
                               from state in ModelState.Values
                               from error in state.Errors
                               select error.ErrorMessage;

                ErrorResponse e = new ErrorResponse();
                e.errors = modelStateErrors.ToList();


                return Request.CreateResponse(HttpStatusCode.NotAcceptable, e);
            }
        }

        [HttpPut]
        [Route("api/user")]
        public HttpResponseMessage EditUser(User user)
        {
            if (ModelState.IsValid)
            {
                //if (user.tenDangNhap.Contains(" "))
                //{
                //    ErrorResponse e = new ErrorResponse();
                //    e.errors.Add("Tên đăng nhập chứa ");
                //    return Request.CreateResponse(HttpStatusCode.NotAcceptable, e);
                //}
                //user.matKhau = Helper.Encrypt("123456");
                if (userDao.editUser(user))
                    return Request.CreateResponse(HttpStatusCode.OK, user);
                else
                {
                    ErrorResponse e = new ErrorResponse();
                    e.errors.Add("Có lỗi xảy ra vui lòng thử lại sau!");
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, e);

                }
            }
            else
            {
                //Convert to message
                IEnumerable<string> modelStateErrors =
                               from state in ModelState.Values
                               from error in state.Errors
                               select error.ErrorMessage;

                ErrorResponse e = new ErrorResponse();
                e.errors = modelStateErrors.ToList();


                return Request.CreateResponse(HttpStatusCode.NotAcceptable, e);
            }
        }



        //Kiểm tra đăng nhập
        //[HttpGet]
        //[Route("api/login")]
        //public HttpResponseMessage IsLogin(string username, string pass)
        //{
        //    string kq = "0";
        //    HttpError err = new HttpError();            
        //    kq = userDao.IsValidLogin(username, pass);
        //    //string test = Helper.Encrypt(pass.Replace("'", "''"));
        //    if (kq == "0")
        //    {
        //        err.Message = "Đăng nhập thất bại. Hãy kiểm tra lại thông tin.";
        //        return Request.CreateResponse(HttpStatusCode.NotFound, err);
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK,"");


        //}

        ////Lấy thông tin user
        //[HttpGet]
        //[Route("api/user/single")]
        //public HttpResponseMessage GetInfoUser(string username)
        //{
        //    User retVal = new User();
        //    retVal = userDao.GetInfoUser(username);
        //    if (retVal == null)
        //    {
        //        var message = string.Format("Không có thông tin user name này");
        //        HttpError err = new HttpError(message);
        //        return Request.CreateResponse(HttpStatusCode.NotFound, err);
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, retVal);
        //}


    }
}