﻿using ServerHotel.Models;
using ServerHotel.Ults;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ServerHotel.DAO
{
    public class DatPhongDAO
    {
        public Provider p = new Provider();
        private string cnn = Provider.ConnectionString;
        //get MADATPHONG
        public string getMaDP()
        {

            SqlParameter code = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@code",
                Direction = ParameterDirection.Output,
                Size = 10
            };
            int row = p.ExecuteNonQuery("sp_GetMaDP", CommandType.StoredProcedure,
                                code);
            return code.Value.ToString();

        }
        

        //Đặt phòng
        public bool DatPhong(DatPhongModel dp)
        {
            SqlParameter error = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };
            int row = p.ExecuteNonQuery("sp_DatPhong", CommandType.StoredProcedure,
                       new SqlParameter { ParameterName = "@maDP", Value = dp.maDB },
                       new SqlParameter { ParameterName = "@maLoaiPhong", Value = dp.maLoaiPhong },
                       new SqlParameter { ParameterName = "@maKH", Value = dp.maKH },
                       new SqlParameter { ParameterName = "@ngayBatDau", Value = dp.ngayBatDau },
                       new SqlParameter { ParameterName = "@ngayTraPhong", Value = dp.ngayTraPhong },

                       new SqlParameter { ParameterName = "@ngayDat", Value = dp.ngayDat },
                       new SqlParameter { ParameterName = "@donGia", Value = dp.DonGia },
                       new SqlParameter { ParameterName = "@moTa", Value = dp.moTa },
                       new SqlParameter { ParameterName = "@slDat", Value = dp.slDat },

                                error);

            if (error.Value.ToString().Trim() == "")
                return true;

            return false;
        }

        internal PagingLists getUnconfirmList(int page)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@page", page);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetListDatPhongChuaXacNhan", param);

            if (!dr.HasRows)
                return null;
            PagingLists retVal = new PagingLists();
            while (dr.Read())
            {
                DatPhongModel h = new DatPhongModel();
                h.maDB = dr["maDB"].ToString().Trim();
                h.maKH = dr["maKH"].ToString().Trim();
                h.maLoaiPhong = dr["maLoaiPhong"].ToString().Trim();
                h.slDat = int.Parse(dr["slDat"].ToString());
                h.DonGia = int.Parse(dr["DonGia"].ToString());
                h.tinhTrang = int.Parse(dr["tinhTrang"].ToString());

                h.ngayBatDau = DateTime.Parse( dr["ngayBatDau"].ToString());
                h.ngayTraPhong = DateTime.Parse(dr["ngayTraPhong"].ToString());
                h.ngayDat = DateTime.Parse(dr["ngayDat"].ToString());

                retVal.page = (int)dr["page"];
                retVal.objects.Add(h);
            }

            return retVal;
        }

        internal object getCheckedIn(int page)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@page", page);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetListDatPhongDaNhanPhong", param);

            if (!dr.HasRows)
                return null;
            PagingLists retVal = new PagingLists();
            while (dr.Read())
            {
                DatPhongModel h = new DatPhongModel();
                h.maDB = dr["maDB"].ToString().Trim();
                h.maKH = dr["maKH"].ToString().Trim();
                h.maLoaiPhong = dr["maLoaiPhong"].ToString().Trim();
                h.slDat = int.Parse(dr["slDat"].ToString());
                h.DonGia = int.Parse(dr["DonGia"].ToString());
                h.tinhTrang = int.Parse(dr["tinhTrang"].ToString());

                h.ngayBatDau = DateTime.Parse(dr["ngayBatDau"].ToString());
                h.ngayTraPhong = DateTime.Parse(dr["ngayTraPhong"].ToString());
                h.ngayDat = DateTime.Parse(dr["ngayDat"].ToString());

                retVal.page = (int)dr["page"];
                retVal.objects.Add(h);
            }

            return retVal;
        }

        internal object getConfirmList(int page)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@page", page);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetListDatPhongDaXacNhan", param);

            if (!dr.HasRows)
                return null;
            PagingLists retVal = new PagingLists();
            while (dr.Read())
            {
                DatPhongModel h = new DatPhongModel();
                h.maDB = dr["maDB"].ToString().Trim();
                h.maKH = dr["maKH"].ToString().Trim();
                h.maLoaiPhong = dr["maLoaiPhong"].ToString().Trim();
                h.slDat = int.Parse(dr["slDat"].ToString());
                h.DonGia = int.Parse(dr["DonGia"].ToString());
                h.tinhTrang = int.Parse(dr["tinhTrang"].ToString());

                h.ngayBatDau = DateTime.Parse(dr["ngayBatDau"].ToString());
                h.ngayTraPhong = DateTime.Parse(dr["ngayTraPhong"].ToString());
                h.ngayDat = DateTime.Parse(dr["ngayDat"].ToString());

                retVal.page = (int)dr["page"];
                retVal.objects.Add(h);
            }

            return retVal;
        }

        internal bool cancelOrder(string maDB)
        {
            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };
            int row = p.ExecuteNonQuery("sp_HuyDatPhong", CommandType.StoredProcedure,
                   new SqlParameter { ParameterName = "@maDP", Value = maDB },
                                erorr);
            if (erorr.Value.ToString() == "")
                return true;
            return false;
        }

        internal bool confirmOrder(string orderID)
        {
            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };
            int row = p.ExecuteNonQuery("sp_XacNhanPhong", CommandType.StoredProcedure,
                   new SqlParameter { ParameterName = "@maDP", Value = orderID },
                                erorr);
            if (erorr.Value.ToString() == "")
                return true;
            return false;
        }

        internal object getListDatPhong(int page = 1)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@page", page);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetListDatPhong", param);

            if (!dr.HasRows)
                return null;
            PagingLists retVal = new PagingLists();
            int flag;
            while (dr.Read())
            {
                DatPhongTrangThai h = new DatPhongTrangThai();
                h.maDB = dr["maDB"].ToString().Trim();
                h.maKH = dr["maKH"].ToString().Trim();
                h.maLoaiPhong = dr["maLoaiPhong"].ToString().Trim();
                h.slDat = int.Parse(dr["slDat"].ToString());
                h.DonGia = int.Parse(dr["DonGia"].ToString());
                flag = int.Parse(dr["tinhTrang"].ToString());
                if (flag == 0)
                    h.tinhTrang = "Chưa xác nhận";
                else if (flag == 1)
                    h.tinhTrang = "Đã xác nhận";
                else if (flag == 2)
                    h.tinhTrang = "Đã hủy";
                else if (flag == 3)
                    h.tinhTrang = "Đang ở";
                else
                    h.tinhTrang = "Đã thanh toán";
                h.ngayBatDau = DateTime.Parse(dr["ngayBatDau"].ToString());
                h.ngayTraPhong = DateTime.Parse(dr["ngayTraPhong"].ToString());
                h.ngayDat = DateTime.Parse(dr["ngayDat"].ToString());

                retVal.page = (int)dr["page"];
                retVal.objects.Add(h);
            }

            return retVal;
        }
    }
}