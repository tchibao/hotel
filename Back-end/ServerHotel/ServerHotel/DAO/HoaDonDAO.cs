﻿using ServerHotel.Ults;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ServerHotel.Models;

namespace ServerHotel.DAO
{
    public class HoaDonDAO
    {
        public Provider p = new Provider();
        private string cnn = Provider.ConnectionString;
        //get MADATPHONG
        public string getMaHD()
        {

            SqlParameter code = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@code",
                Direction = ParameterDirection.Output,
                Size = 10
            };
            int row = p.ExecuteNonQuery("sp_GetMaHD", CommandType.StoredProcedure,
                                code);
            return code.Value.ToString();

        }

        public PagingLists getListHoaDon(int page)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@page", page);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetListHoaDon", param);

            if (!dr.HasRows)
            {
                return null;
            }
            PagingLists retVal = new PagingLists();
            while (dr.Read())
            {
                HoaDonModel h = new HoaDonModel();
                h.maHD = dr["maHD"].ToString().Trim();
                h.ngayThanhToan = DateTime.Parse(dr["ngayThanhToan"].ToString());
                h.TongTien = int.Parse(dr["TongTien"].ToString());
                h.maDP = dr["maDP"].ToString();

                retVal.objects.Add(h);
                retVal.page = (int)dr["page"];
            }

            return retVal;
        }

        public HoaDonModel getInfoHoaDon(string maHD)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@maHD", maHD);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetInfoHoaDon", param);

            if (!dr.HasRows)
            {
                return null;
            }
            HoaDonModel retVal = new HoaDonModel();
            while (dr.Read())
            {
                retVal.maHD = dr["maHD"].ToString().Trim();
                retVal.ngayThanhToan = DateTime.Parse(dr["ngayThanhToan"].ToString());
                retVal.TongTien = int.Parse(dr["TongTien"].ToString());
                retVal.maDP = dr["maDP"].ToString();
            }

            return retVal;
        }
    }
}