﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServerHotel.Models;
using ServerHotel.Ults;
using System.Data;
using System.Data.SqlClient;

namespace ServerHotel.DAO
{
    public class HotelDAO
    {
        public Provider p = new Provider();
        private string cnn = Provider.ConnectionString;

        public bool addNewHotel(Hotel hotel)
        {
            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };
            int row = p.ExecuteNonQuery("sp_AddNewHotel", CommandType.StoredProcedure,
                                new SqlParameter { ParameterName = "@maKS", Value = hotel.maKS },
                                new SqlParameter { ParameterName = "@tenKS", Value = hotel.tenKS },
                                new SqlParameter { ParameterName = "@soSao", Value = hotel.soSao },
                                new SqlParameter { ParameterName = "@soNha", Value = hotel.soNha },
                                new SqlParameter { ParameterName = "@duong", Value = hotel.duong },
                                new SqlParameter { ParameterName = "@quan", Value = hotel.quan },
                                new SqlParameter { ParameterName = "@thanhPho", Value = hotel.thanhPho },
                                new SqlParameter { ParameterName = "@giaTB", Value = hotel.giaTB },
                                new SqlParameter { ParameterName = "@moTa", Value = hotel.moTa },
                                new SqlParameter { ParameterName = "@avatar", Value = hotel.avatar },
                                erorr);
            if (erorr.Value.ToString().Trim() == "")
                return true;
            return false;
            //string sqlQuery = $"Insert into KHACHSAN VALUES  ('{hotel.maKS}','{hotel.tenKS}',{hotel.soSao},'{hotel.soNha}','{hotel.duong}','{hotel.quan}','{hotel.thanhPho}',{hotel.giaTB},'{hotel.moTa}','{hotel.avatar}')  ";
            //System.Diagnostics.Debug.WriteLine(sqlQuery);

            //int dr = SqlHelper.ExecuteNonQuery(cnn, CommandType.Text, sqlQuery);
            //if (dr > 0)
            //{
            //    return true;
            //}
            
            //return false;
        }

        public bool updateHotel(Hotel hotel)
        {
            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };
            int row = p.ExecuteNonQuery("sp_UpdateHotel", CommandType.StoredProcedure,
                                new SqlParameter { ParameterName = "@maKS", Value = hotel.maKS },
                                new SqlParameter { ParameterName = "@tenKS", Value = hotel.tenKS },
                                new SqlParameter { ParameterName = "@soSao", Value = hotel.soSao },
                                new SqlParameter { ParameterName = "@soNha", Value = hotel.soNha },
                                new SqlParameter { ParameterName = "@duong", Value = hotel.duong },
                                new SqlParameter { ParameterName = "@quan", Value = hotel.quan },
                                new SqlParameter { ParameterName = "@thanhPho", Value = hotel.thanhPho },
                                new SqlParameter { ParameterName = "@giaTB", Value = hotel.giaTB },
                                new SqlParameter { ParameterName = "@moTa", Value = hotel.moTa },
                                new SqlParameter { ParameterName = "@avatar", Value = hotel.avatar },
                                erorr);
            if (erorr.Value.ToString().Trim() == "")
                return true;
            return false;
        }

        public bool deleteHotel(string maKS)
        {
            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };
            int row = p.ExecuteNonQuery("sp_DeleteHotel", CommandType.StoredProcedure,
                                new SqlParameter { ParameterName = "@maKS", Value = maKS },
                                erorr);
            if (erorr.Value.ToString().Trim() == "")
                return true;
            return false;
        }


        public bool isHotel(string maKS)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@maKS", maKS);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_IsHotel", param);
            while (dr.Read())
            {
                return false;
            }
            return true;
        }

        public HotelPaging getListHotel(int page)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@page", page);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetListHotel", param);

            HotelPaging retVal = new HotelPaging();
            while (dr.Read())
            {
                Hotel h = new Hotel();
                h.maKS = dr["maKS"].ToString();
                h.maKS = h.maKS.Trim();
                h.tenKS = dr["tenKS"].ToString();
                h.soSao = float.Parse(dr["soSao"].ToString());
                h.soNha = dr["soNha"].ToString();
                h.duong = dr["duong"].ToString();
                h.quan = dr["quan"].ToString();
                string avatarID = dr["avatar"].ToString();
                h.avatar = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", avatarID);
                h.thanhPho = dr["thanhPho"].ToString();
                h.moTa = dr["moTa"].ToString();
                h.giaTB = (int)dr["giaTB"];
                retVal.page = (int)dr["page"];
                retVal.hotel.Add(h);
            }

            return retVal;
        }

        public bool checkCanDelete(string maKS)
        {
            SqlParameter value = new SqlParameter
            {
                DbType = DbType.Int16,
                ParameterName = "@value",
                Direction = ParameterDirection.Output,
  
            };

            int row = p.ExecuteNonQuery("sp_CheckDeleteKhachSan", CommandType.StoredProcedure,
                                new SqlParameter { ParameterName = "@maKS", Value = maKS },
                                new SqlParameter { ParameterName = "@ngayHienTai", Value = DateTime.Now },

                                value);
            if (value.Value.ToString().Trim() == "1")
                return true;
            return false;
        }
        public Hotel getHotel(string id)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@maKS", id);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetHotel", param);

            if (!dr.HasRows)
            {
                return null;
            }
            Hotel retVal = null;
            while (dr.Read())
            {
                retVal = new Hotel();
                retVal.maKS = dr["maKS"].ToString().Trim();
                retVal.tenKS = dr["tenKS"].ToString();
                retVal.soSao = float.Parse(dr["soSao"].ToString());
                retVal.soNha = dr["soNha"].ToString();
                retVal.duong = dr["duong"].ToString();
                retVal.quan = dr["quan"].ToString();
                retVal.thanhPho = dr["thanhPho"].ToString();
                retVal.moTa = dr["moTa"].ToString();
                retVal.giaTB = (int)dr["giaTB"];
                retVal.avatarID = dr["avatar"].ToString().Trim();
                retVal.avatar = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", retVal.avatarID);
            }

            return retVal;
        }

        //Tìm kiếm khách sạn theo nhiều tiêu chí
        public HotelPaging FindListHotel(int soTien1, int soTien2, int soSao1, int soSao2, string thanhPho, int page)
        {
            var param = new object[6];
            param[0] = new SqlParameter("@giaTB1", soTien1);
            param[1] = new SqlParameter("@giaTB2", soTien2);
            param[2] = new SqlParameter("@soSao1", soSao1);
            param[3] = new SqlParameter("@soSao2", soSao2);
            param[4] = new SqlParameter("@thanhPho", thanhPho);
            param[5] = new SqlParameter("@page", page);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_FindListHotel", param);

            HotelPaging retVal = new HotelPaging();
            while (dr.Read())
            {
                Hotel h = new Hotel();
                h.maKS = dr["maKS"].ToString().Trim();
                h.tenKS = dr["tenKS"].ToString();
                h.soSao = float.Parse(dr["soSao"].ToString());
                h.soNha = dr["soNha"].ToString();
                h.duong = dr["duong"].ToString();
                h.quan = dr["quan"].ToString();
                h.thanhPho = dr["thanhPho"].ToString();
                h.moTa = dr["moTa"].ToString();
                h.giaTB = (int)dr["giaTB"];
                retVal.page = (int)dr["page"];
                h.avatarID = dr["avatar"].ToString().Trim();
                h.avatar = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", h.avatarID);
                retVal.hotel.Add(h);
            }

            return retVal;
        }

        //Tìm kiếm khách sạn theo thành phố
        public HotelPaging FindListHotel(string thanhPho ,int page)
        {
            var param = new object[2];
            param[0] = new SqlParameter("@thanhPho", thanhPho);
            param[1] = new SqlParameter("@page", page);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_FindListHotel2", param);

            HotelPaging retVal = new HotelPaging();
            while (dr.Read())
            {
                Hotel h = new Hotel();
                h.maKS = dr["maKS"].ToString().Trim();
                h.tenKS = dr["tenKS"].ToString();
                h.soSao = float.Parse(dr["soSao"].ToString());
                h.soNha = dr["soNha"].ToString();
                h.duong = dr["duong"].ToString();
                h.quan = dr["quan"].ToString();
                h.thanhPho = dr["thanhPho"].ToString();
                h.moTa = dr["moTa"].ToString();
                h.giaTB = (int)dr["giaTB"];
                h.avatarID = dr["avatar"].ToString().Trim();
                h.avatar = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", h.avatarID);
                retVal.page = (int)dr["page"];
                retVal.hotel.Add(h);
            }

            return retVal;
        }

        public HotelPaging FindListHotelAdmin(string query, int page)
        {
            var param = new object[2];
            param[0] = new SqlParameter("@strSearch", query);
            param[1] = new SqlParameter("@page", page);
        

            var dr = SqlHelper.ExecuteReader(cnn, "sp_FindListHotelTheoNameAndCode", param);

            HotelPaging retVal = new HotelPaging();
            while (dr.Read())
            {
                Hotel h = new Hotel();
                h.maKS = dr["maKS"].ToString().Trim();
                h.tenKS = dr["tenKS"].ToString();
                h.soSao = float.Parse(dr["soSao"].ToString());
                h.soNha = dr["soNha"].ToString();
                h.duong = dr["duong"].ToString();
                h.quan = dr["quan"].ToString();
                h.thanhPho = dr["thanhPho"].ToString();
                h.moTa = dr["moTa"].ToString();
                h.giaTB = (int)dr["giaTB"];
                h.avatarID = dr["avatar"].ToString().Trim();
                h.avatar = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", h.avatarID);
                retVal.page = (int)dr["page"];
                retVal.hotel.Add(h);
            }

            return retVal;
        }

    }
}