﻿using ServerHotel.Models;
using ServerHotel.Ults;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ServerHotel.DAO
{
    public class LoaiPhongDAO
    {
        public Provider p = new Provider();
        private string cnn = Provider.ConnectionString;


        public bool isRoomType(string maLoaiphong)
        {
            string sqlQuery = $"Select * FROM LOAIPHONG WHERE maLoaiPhong =  '{maLoaiphong}' ";
            var dr = SqlHelper.ExecuteReader(cnn, System.Data.CommandType.Text, sqlQuery);


            while (dr.HasRows)
            {
                return false;
            }
            return true;
        }
        public bool addRoomType(LoaiPhong loaiphong)
        {
            string sqlQuery = $"Insert into LoaiPhong VALUES  ('{loaiphong.maLoaiPhong}','{loaiphong.tenLoaiPhong}','{loaiphong.maKS}'," +
                $"{loaiphong.donGia},'{loaiphong.moTa}',{loaiphong.slTrong},'{loaiphong.testImg}','{loaiphong.detailImage1}','{loaiphong.detailImage2}','{loaiphong.detailImage3}','{loaiphong.detailImage4}',1)  ";
            //System.Diagnostics.Debug.WriteLine(sqlQuery);

            int dr = SqlHelper.ExecuteNonQuery(cnn, CommandType.Text, sqlQuery);

            //SqlParameter value = new SqlParameter
            //{
            //    DbType = DbType.String,
            //    ParameterName = "@erorr",
            //    Direction = ParameterDirection.Output,

            //};

            //int row = p.ExecuteNonQuery("sp_AddNewLoaiPhong", CommandType.StoredProcedure,
            //                    new SqlParameter { ParameterName = "@maLoaiPhong", Value = loaiphong.maLoaiPhong },
            //                    new SqlParameter { ParameterName = "@tenLoaiPhong", Value = loaiphong.tenLoaiPhong },
            //                    new SqlParameter { ParameterName = "@maKS", Value = loaiphong.maKS },
            //                    new SqlParameter { ParameterName = "@donGia", Value = loaiphong.donGia },
            //                    new SqlParameter { ParameterName = "@moTa", Value = loaiphong.moTa },
            //                    new SqlParameter { ParameterName = "@slTrong", Value = loaiphong.slTrong },
            //                    new SqlParameter { ParameterName = "@testImg", Value = loaiphong.testImg },
            //                    new SqlParameter { ParameterName = "@detailImage1", Value = loaiphong.detailImage1 },
            //                    new SqlParameter { ParameterName = "@detailImage2", Value = loaiphong.detailImage2 },
            //                    new SqlParameter { ParameterName = "@detailImage3", Value = loaiphong.detailImage3 },
            //                    new SqlParameter { ParameterName = "@detailImage4", Value = loaiphong.detailImage4 },

            //                    value);


            if (dr > 0)
            {
                return true;
            }

            return false;

        }
        public List<LoaiPhong> getList(string maKS,int page)
        {
            var param = new object[2];
            param[1] = new SqlParameter("@page", page);
            param[0] = new SqlParameter("@maHotel", maKS);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetListLoaiPhong",
                param);



            //string sqlQuery = $"Select * FROM LOAIPHONG WHERE maKS =  '{maKS}' ";
            //var dr = SqlHelper.ExecuteReader(cnn, System.Data.CommandType.Text, sqlQuery);

            if (!dr.HasRows)
            {
                return null;
            }
            List<LoaiPhong> retVal = new List<LoaiPhong>();
            while (dr.Read())
            {
                LoaiPhong h = new LoaiPhong();
                h.maKS = dr["maKS"].ToString().Trim();
                h.maLoaiPhong = dr["maLoaiPhong"].ToString().Trim();
                h.donGia = int.Parse(dr["donGia"].ToString());
                h.tenLoaiPhong = dr["tenLoaiPhong"].ToString();
                h.slTrong = int.Parse(dr["slTrong"].ToString());


                h.moTa = dr["moTa"].ToString();
                string imgID = dr["testImg"].ToString();
                //
                h.testImg = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", imgID);

                 imgID = dr["detailImage1"].ToString();
                h.detailImage1 = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", imgID);

                imgID = dr["detailImage2"].ToString();
                h.detailImage2 = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", imgID);

                imgID = dr["detailImage3"].ToString();
                h.detailImage3 = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", imgID);

                imgID = dr["detailImage4"].ToString();
                h.detailImage4 = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", imgID);


                retVal.Add(h);


            }

            return retVal;
        }

        internal bool checkCanDelete(string roomtypeID)
        {
            SqlParameter value = new SqlParameter
            {
                DbType = DbType.Int16,
                ParameterName = "@value",
                Direction = ParameterDirection.Output,

            };

            int row = p.ExecuteNonQuery("sp_CheckDeleteKhachSan", CommandType.StoredProcedure,
                                new SqlParameter { ParameterName = "@maKS", Value = roomtypeID },
                                new SqlParameter { ParameterName = "@ngayHienTai", Value = DateTime.Now },

                                value);
            if (value.Value.ToString().Trim() == "1")
                return true;
            return false;
        }

        internal bool deleteRoomType(string roomtypeID)
        {
            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };
            int row = p.ExecuteNonQuery("sp_DeleteLoaiPhong", CommandType.StoredProcedure,
                                new SqlParameter { ParameterName = "@maLoaiPhong", Value = roomtypeID },
                                erorr);
            if (erorr.Value.ToString().Trim() == "")
                return true;
            return false;
        }

        public LoaiPhong getDetail(string maPhong)
        {


            string sqlQuery = $"Select * FROM LOAIPHONG WHERE maLoaiPhong =  '{maPhong}' ";
            var dr = SqlHelper.ExecuteReader(cnn, System.Data.CommandType.Text, sqlQuery);
            if (!dr.HasRows)
            {
                return null;
            }
            LoaiPhong retVal = new LoaiPhong();
            while (dr.Read())
            {
                LoaiPhong h = new LoaiPhong();
                h.maKS = dr["maKS"].ToString().Trim();
                h.maLoaiPhong = dr["maLoaiPhong"].ToString().Trim();
                h.donGia = int.Parse(dr["donGia"].ToString());
                h.tenLoaiPhong = dr["tenLoaiPhong"].ToString();
                h.slTrong = int.Parse(dr["slTrong"].ToString());


                h.moTa = dr["moTa"].ToString();
                string imgID = dr["testImg"].ToString();
                //
                // h.testImg = string.Format("http://res.cloudinary.com/doga4u6vf/image/upload/v1513615928/{0}.jpg", imgID);
                h.testImg = imgID;
                imgID = dr["detailImage1"].ToString();
                h.detailImage1 = imgID;

                imgID = dr["detailImage2"].ToString();
                h.detailImage2 = imgID;

                imgID = dr["detailImage3"].ToString();
                h.detailImage3 = imgID;

                imgID = dr["detailImage4"].ToString();
                h.detailImage4 = imgID;
                //Load thêm hình detail
                return h;


            }

            return retVal;
        }


        public bool updateLoaiPhong(LoaiPhong loaiPhong)
        {
            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };
            int row = p.ExecuteNonQuery("sp_UpdateLoaiPhong", CommandType.StoredProcedure,
                                new SqlParameter { ParameterName = "@maLoaiPhong", Value = loaiPhong.maLoaiPhong },
                                new SqlParameter { ParameterName = "@tenLoaiPhong", Value = loaiPhong.tenLoaiPhong },
                                new SqlParameter { ParameterName = "@maKS", Value = loaiPhong.maKS },
                                new SqlParameter { ParameterName = "@donGia ", Value = loaiPhong.donGia },
                                new SqlParameter { ParameterName = "@moTa", Value = loaiPhong.moTa },
                                new SqlParameter { ParameterName = "@slTrong", Value = loaiPhong.slTrong },
                                new SqlParameter { ParameterName = "@testImg", Value = loaiPhong.testImg },
                                new SqlParameter { ParameterName = "@detailImage1", Value = loaiPhong.detailImage1 },
                                new SqlParameter { ParameterName = "@detailImage2", Value = loaiPhong.detailImage2 },
                                new SqlParameter { ParameterName = "@detailImage3", Value = loaiPhong.detailImage3 },
                                new SqlParameter { ParameterName = "@detailImage4", Value = loaiPhong.detailImage4 },

                                erorr);
            if (erorr.Value.ToString().Trim() == "")
                return true;
            return false;
        }
        //public bool updateLoaiPhong(LoaiPhong loaiphong)
        //{
        //    string sqlQuery = $"Update LoaiPhong Set [tenLoaiPhong]
        //    System.Diagnostics.Debug.WriteLine(sqlQuery);

        //    int dr = SqlHelper.ExecuteNonQuery(cnn, CommandType.Text, sqlQuery);
        //    if (dr > 0)
        //    {
        //        return true;
        //    }

        //    return false;
        //}

    }
}