﻿using ServerHotel.Models;
using ServerHotel.Ults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerHotel.DAO
{
    public class OtherDAO
    {
        public Provider p = new Provider();
        private string cnn = Provider.ConnectionString;



        public List<ThanhPho> getThanhPho()
        {

            string sqlQuery = $"Select * FROM ThanhPho";
            var dr = SqlHelper.ExecuteReader(cnn, System.Data.CommandType.Text, sqlQuery);

            if (!dr.HasRows)
            {
                return null;
            }
            List<ThanhPho> retVal = new List<ThanhPho>();
            while (dr.Read())
            {
                ThanhPho h = new ThanhPho();
                h.maTP = dr["maTP"].ToString().Trim();
                h.tenTP = dr["tenTP"].ToString().Trim();
  

                retVal.Add(h);


            }

            return retVal;
        }



        public List<Quan> getQuan(string maTP)
        {

            string sqlQuery = $"Select * FROM Quan where maTP ='{maTP}' ";
            var dr = SqlHelper.ExecuteReader(cnn, System.Data.CommandType.Text, sqlQuery);

            if (!dr.HasRows)
            {
                return null;
            }
            List<Quan> retVal = new List<Quan>();
            while (dr.Read())
            {
                Quan h = new Quan();
                h.maTP = dr["maTP"].ToString().Trim();
                h.quan = dr["quan"].ToString().Trim();


                retVal.Add(h);


            }

            return retVal;
        }

    }
}