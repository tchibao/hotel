﻿using ServerHotel.Models;
using ServerHotel.Ults;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ServerHotel.DAO
{
    public class PhongDAO
    {
        public Provider p = new Provider();
        private string cnn = Provider.ConnectionString;

        public bool CheckIn(ListPhongTest list)
        {
            DataTable dt = new DataTable();
            //Xem thử cái List_Phong gồm những cột gì thì khai báo
            dt.Columns.Add("maPhong", typeof(string));
            dt.Columns.Add("ngay",typeof(DateTime));
            dt.Columns.Add("tinhTrang", typeof(int));
            dt.Columns.Add("maDP", typeof(string));

            DateTime d = DateTime.Today;
            //thêm vào list
            for (int i = 0; i < list.lstPhong.Length; i++)
            { 
                string test = list.lstPhong[i];
                dt.Rows.Add(test,d,0,list.maDP);
            }
            //string error = boDeThiBUS.ThemBoDeThi(boDeThiDTO, dt);
            //if (error.Trim() != "")
            //{
            //    System.Windows.MessageBox.Show(error.Trim(), "Thông báo");
            //    return;
            //}
          
            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };
            SqlParameter lsPhong = new SqlParameter("@List_Phong", SqlDbType.Structured)
            {
               TypeName = "dbo.List_Phong",
                Value = dt
            };

            int row =p.ExecuteNonQuery("sp_NhanPhong", CommandType.StoredProcedure,
                            new SqlParameter { ParameterName = "@maDP", Value = list.maDP },
                          new SqlParameter { ParameterName = "@ngayBatDau", Value = d },
                         lsPhong, erorr);
            if (erorr.Value.ToString() == "")
            {
                return true;
            }
            return false;


        }

        public PagingLists getList(int page,string maKS)
        {

            var param = new object[2];
            param[0] = new SqlParameter("@maKS", maKS);

            param[1] = new SqlParameter("@page", page);


            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetListPhongTheoKhachSan", param);



            //string sqlQuery = $"Select * FROM PHONG WHERE loaiPhong =  '{maLoaiPhong}' ";
            //var dr = SqlHelper.ExecuteReader(cnn, System.Data.CommandType.Text, sqlQuery);

            if (!dr.HasRows)
            {
                return null;
            }
           // List<Phong> retVal = new List<Phong>();
            PagingLists List = new PagingLists();

            while (dr.Read())
            {
                Phong h = new Phong();
                h.maPhong = dr["maPhong"].ToString().Trim();
                h.loaiPhong = dr["loaiPhong"].ToString().Trim();
                h.daDat = int.Parse(dr["daDat"].ToString());
                h.isDelete = dr["isDelete"].ToString() == "False" ? true : false;
               // retVal.Add(h);
                List.objects.Add(h);
                List.page =  (int)dr["page"];
            }
          
            return List;
        }

        internal HoaDonModel CheckOut(CheckOutPostModel model)
        {
            HoaDonDAO hdDao = new HoaDonDAO();
            HoaDonModel retVal = new HoaDonModel()
            {
                maHD = hdDao.getMaHD(),
                TongTien = model.tongTien,
                ngayThanhToan = DateTime.Today,
                maDP = model.maDP
            };



            DataTable dt = new DataTable();
            //Xem thử cái List_Phong gồm những cột gì thì khai báo
            dt.Columns.Add("maPhong", typeof(string));
            dt.Columns.Add("ngay", typeof(DateTime));
            dt.Columns.Add("tinhTrang", typeof(int));
            dt.Columns.Add("maDP", typeof(string));

            DateTime d = DateTime.Today;
            //thêm vào list
            for (int i = 0; i < model.lstPhong.Length; i++)
            {
                string test = model.lstPhong[i];
                dt.Rows.Add(test, d, 0, model.maDP);
            }
            //string error = boDeThiBUS.ThemBoDeThi(boDeThiDTO, dt);
            //if (error.Trim() != "")
            //{
            //    System.Windows.MessageBox.Show(error.Trim(), "Thông báo");
            //    return;
            //}

            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };
            SqlParameter lsPhong = new SqlParameter("@List_Phong", SqlDbType.Structured)
            {
                TypeName = "dbo.List_Phong",
                Value = dt
            };

            int row = p.ExecuteNonQuery("sp_TraPhong", CommandType.StoredProcedure,
                            new SqlParameter { ParameterName = "@maHD", Value = retVal.maHD },
                            new SqlParameter { ParameterName = "@ngayThanhToan", Value = retVal.ngayThanhToan },
                            new SqlParameter { ParameterName = "@tongTien", Value = retVal.TongTien },
                            new SqlParameter { ParameterName = "@maDP", Value = retVal.maDP },
                            new SqlParameter { ParameterName = "@ngayTraPhong", Value = retVal.ngayThanhToan },

                         lsPhong, erorr);
            if (erorr.Value.ToString() == "")
            {
                return retVal;
            }
            return null;
        }

        internal List<string>  GetListPhong(DatPhongModel model,out DateTime? bookRoom)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@maDP", model.maDB);


            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetListPhongTheoMaDP", param);

            bookRoom = null;

            //string sqlQuery = $"Select * FROM PHONG WHERE loaiPhong =  '{maLoaiPhong}' ";
            //var dr = SqlHelper.ExecuteReader(cnn, System.Data.CommandType.Text, sqlQuery);

            if (!dr.HasRows)
            {
                return null;
            }
            List<string> retVal = new List<string>();
            while (dr.Read())
            {
               string maPhong = dr["maPhong"].ToString().Trim();
                bookRoom =  DateTime.Parse( dr["ngay"].ToString());
                retVal.Add(maPhong);

            }

            return retVal;
        }

        //lay danh sach phong theo hoa don
        internal List<string> GetListPhongTheoHoaDon(HoaDonModel model)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@maDP", model.maDP);


            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetListPhongTheoMaDP", param);

            //string sqlQuery = $"Select * FROM PHONG WHERE loaiPhong =  '{maLoaiPhong}' ";
            //var dr = SqlHelper.ExecuteReader(cnn, System.Data.CommandType.Text, sqlQuery);

            if (!dr.HasRows)
            {
                return null;
            }
            List<string> retVal = new List<string>();
            while (dr.Read())
            {
                string maPhong = dr["maPhong"].ToString().Trim();
                retVal.Add(maPhong);

            }

            return retVal;
        }

        public List<Phong> GetListPhongChuaDatTheoLoaiPhong(string maLoaiPhong)
        {

            var param = new object[1];
            param[0] = new SqlParameter("@maLoaiPhong", maLoaiPhong);


            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetListPhongChuaDatTheoLoaiPhong", param);



            //string sqlQuery = $"Select * FROM PHONG WHERE loaiPhong =  '{maLoaiPhong}' ";
            //var dr = SqlHelper.ExecuteReader(cnn, System.Data.CommandType.Text, sqlQuery);

            if (!dr.HasRows)
            {
                return null;
            }
            List<Phong> retVal = new List<Phong>();
            while (dr.Read())
            {
                Phong h = new Phong();
                h.maPhong = dr["maPhong"].ToString().Trim();
                h.loaiPhong = dr["loaiPhong"].ToString().Trim();
                h.daDat = int.Parse(dr["daDat"].ToString());
                h.isDelete = dr["isDelete"].ToString() == "False" ? true : false;
                retVal.Add(h);

            }

            return retVal;
        }


        public bool addRoom(Phong phong)
        {
            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };
            int row = p.ExecuteNonQuery("sp_AddNewPhong", CommandType.StoredProcedure,
                                new SqlParameter { ParameterName = "@maPhong", Value = phong.maPhong },
                                new SqlParameter { ParameterName = "@loaiPhong", Value = phong.loaiPhong },
                              

                                erorr);
            if (erorr.Value.ToString().Trim() == "")
                return true;
            return false;

        }
    }


}