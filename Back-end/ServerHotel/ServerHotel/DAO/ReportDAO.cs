﻿using ServerHotel.Models;
using ServerHotel.Ults;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ServerHotel.DAO
{
    public class ReportDAO
    {
        public Provider p = new Provider();
        private string cnn = Provider.ConnectionString;

        public List<Report> getReportByYear(int year)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@nam", year);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_BaoCaoDoanhThuTheoNam", param);

            List<Report> retVal = new List<Report>();
            for(int i = 0; i < 12; i++)
            {
                Report r = new Report()
                {
                    X = "Tháng " + (i + 1),
                    Y = 0
                };
                retVal.Add(r);
            }
            while (dr.Read())
            {

                int x= int.Parse( dr["thang"].ToString());
                long y = long.Parse( dr["tongTien"].ToString());

                retVal[x - 1].Y = y;
            }


            return retVal;
        }
        public List<Report> getReportByDate(DateTime startDate, DateTime endDate)
        {
            var param = new object[2];
            param[0] = new SqlParameter("@tuNgay", startDate);
            param[1] = new SqlParameter("@denNgay", endDate);


            var dr = SqlHelper.ExecuteReader(cnn, "sp_BaoCaoDoanhThuTheoThang", param);

            List<Report> retVal = new List<Report>();
            //for (int i = 0; i < 12; i++)
            //{
            //    Report r = new Report()
            //    {
            //        X = "Tháng " + (i + 1),
            //        Y = 0
            //    };
            //    retVal.Add(r);
            //}
            while (dr.Read())
            {

                string x = dr["ngayThanhToan"].ToString();
                long y = long.Parse(dr["tongTien"].ToString());
                Report r = new Report()
                {
                    X = x,
                    Y = y
                };
                retVal.Add(r);
            }


            return retVal;
        }
    }
}