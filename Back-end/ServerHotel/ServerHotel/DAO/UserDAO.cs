﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServerHotel.Ults;
using System.Data;
using System.Data.SqlClient;
using ServerHotel.Models;

namespace ServerHotel.DAO
{
    public class UserDAO
    {
        public static Provider p = new Provider();
        private string cnn = Provider.ConnectionString;

        //Kiểm tra đăng nhập
        //public string IsValidLogin(string username, string pass)
        //{
        //    SqlParameter erorr = new SqlParameter
        //    {
        //        DbType = DbType.String,
        //        ParameterName = "@out",
        //        Direction = ParameterDirection.Output,
        //        Size = 1
        //    };

        //    int row = p.ExecuteNonQuery("sp_IsLogin", CommandType.StoredProcedure,
        //                    new SqlParameter { ParameterName = "@tenDangNhap", Value = username.Replace("'", "''") },
        //                    new SqlParameter { ParameterName = "@matKhau", Value = Helper.Encrypt(pass.Replace("'", "''")) },
        //                    erorr);

        //    return erorr.Value.ToString();
        //}

        public User IsValidLogin(string username, string pass)
        {
            User user = new User();

            var param = new object[2];
            param[0] = new SqlParameter("@tenDanhNhap", username);
            param[1] = new SqlParameter { ParameterName = "@matKhau", Value = Helper.Encrypt(pass.Replace("'", "''")) };
           

            var rs = SqlHelper.ExecuteReader(cnn, "sp_IsLogin", param);
            if (!rs.HasRows)
            {
                return null;
            }
            while (rs.Read())
            {
                user.maNguoiDung = rs["maNguoiDung"].ToString();
                user.hoTen = rs["hoTen"].ToString();
                user.tenDangNhap = username;
                user.matKhau = rs["matKhau"].ToString();
                user.soCMND = rs["soCMND"].ToString();
                user.diaChi = rs["diaChi"].ToString();
                user.soDienThoai = rs["soDienThoai"].ToString();
                user.moTa = rs["moTa"].ToString();
                user.email = rs["email"].ToString();
                user.loaiNguoiDung = (int)rs["loaiNguoiDung"];
            }

            return user;
        }


        //Kiểm tra người dùng này đã có chưa
        public string IsValidPass(string username)
        {
            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@out",
                Direction = ParameterDirection.Output,
                Size = 1
            };
            int row = p.ExecuteNonQuery("sp_IsValidPass", CommandType.StoredProcedure,
                            new SqlParameter { ParameterName = "@tenDangNhap", Value = username },
                            erorr);

            return erorr.Value.ToString();
        }

        public User GetInfoUser(string username)
        {
            User user = new User();
            var param = new object[1];
            param[0] = new SqlParameter("@tenDanhNhap", username);

            var rs = SqlHelper.ExecuteReader(cnn, "sp_GetInfoUser", param);
            if (!rs.HasRows)
            {
                return null;
            }
            while (rs.Read())
            {
                user.maNguoiDung = rs["maNguoiDung"].ToString();
                user.hoTen = rs["hoTen"].ToString();
                user.tenDangNhap = username;
                user.matKhau = rs["matKhau"].ToString();
                user.soCMND = rs["soCMND"].ToString();
                user.diaChi = rs["diaChi"].ToString();
                user.soDienThoai = rs["soDienThoai"].ToString();
                user.moTa = rs["moTa"].ToString();
                user.email = rs["email"].ToString();
                user.loaiNguoiDung = (int)rs["loaiNguoiDung"];
            }

            return user;
        }

        public PagingLists getListUser(int page)
        {
            var param = new object[1];
            param[0] = new SqlParameter("@page", page);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetListUser", param);

            PagingLists retVal = new PagingLists();
            while (dr.Read())
            {
                User h = new User();
                h.maNguoiDung = dr["maNguoiDung"].ToString();
                h.hoTen = dr["hoTen"].ToString();
                h.tenDangNhap = dr["tenDangNhap"].ToString();
                h.soCMND = dr["soCMND"].ToString();
                h.diaChi = dr["diaChi"].ToString();
                h.soDienThoai = dr["soDienThoai"].ToString();
         
                h.email = dr["email"].ToString();
                h.moTa = dr["moTa"].ToString();
                h.isDelete = dr["isDelete"].ToString() == "False" ? true: false;
                h.loaiNguoiDung = (int)dr["loaiNguoiDung"];

                retVal.page = (int)dr["page"];
                retVal.objects.Add(h);
            }

            return retVal;
        }


        public bool addUser(User user)
        {
            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };

            int row = p.ExecuteNonQuery("sp_AddNewUser", CommandType.StoredProcedure,
                                new SqlParameter { ParameterName = "@maNguoiDung", Value = user.tenDangNhap },
                                new SqlParameter { ParameterName = "@hoten", Value = user.hoTen },
                                new SqlParameter { ParameterName = "@tenDangNhap", Value = user.tenDangNhap },
                                new SqlParameter { ParameterName = "@matKhau", Value = user.matKhau },
                                new SqlParameter { ParameterName = "@soCMND", Value = user.soCMND },
                                new SqlParameter { ParameterName = "@diachi", Value = user.diaChi },
                                new SqlParameter { ParameterName = "@moTa", Value = user.moTa },

                                new SqlParameter { ParameterName = "@soDienThoai", Value = user.soDienThoai },
                                new SqlParameter { ParameterName = "@email", Value = user.email },
                                new SqlParameter { ParameterName = "@loaiNguoiDung", Value = user.loaiNguoiDung },
                                erorr);
            if (erorr.Value.ToString().Trim() == "")
                return true;
            return false;
            //string sqlQuery = $"Insert into KHACHSAN VALUES  ('{hotel.maKS}','{hotel.tenKS}',{hotel.soSao},'{hotel.soNha}','{hotel.duong}','{hotel.quan}','{hotel.thanhPho}',{hotel.giaTB},'{hotel.moTa}','{hotel.avatar}')  ";
            //System.Diagnostics.Debug.WriteLine(sqlQuery);

            //int dr = SqlHelper.ExecuteNonQuery(cnn, CommandType.Text, sqlQuery);
            //if (dr > 0)
            //{
            //    return true;
            //}

            //return false;
        }

        internal User getDetail(string id)
        {

            var param = new object[1];
            param[0] = new SqlParameter("@tenDangNhap", id);

            var dr = SqlHelper.ExecuteReader(cnn, "sp_GetInfoUser", param);

            if (!dr.HasRows)
                return null;

            dr.Read();
            User h = new User();
            h.maNguoiDung = dr["maNguoiDung"].ToString();
            h.hoTen = dr["hoTen"].ToString();
            h.tenDangNhap = dr["tenDangNhap"].ToString();
            h.soCMND = dr["soCMND"].ToString();
            h.diaChi = dr["diaChi"].ToString();
            h.soDienThoai = dr["soDienThoai"].ToString();

            h.email = dr["email"].ToString();
            h.moTa = dr["moTa"].ToString();
            h.isDelete = dr["isDelete"].ToString() == "False" ? true : false;
            h.loaiNguoiDung = (int)dr["loaiNguoiDung"];

     

            return h;
        }

        internal bool editUser(User user)
        {
            SqlParameter erorr = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = "@erorr",
                Direction = ParameterDirection.Output,
                Size = 200
            };

            int row = p.ExecuteNonQuery("sp_UpdateUser", CommandType.StoredProcedure,
                                new SqlParameter { ParameterName = "@maNguoiDung", Value = user.maNguoiDung },
                                new SqlParameter { ParameterName = "@hoten", Value = user.hoTen },
                                new SqlParameter { ParameterName = "@tenDangNhap", Value = user.tenDangNhap },
                                new SqlParameter { ParameterName = "@soCMND", Value = user.soCMND },
                                new SqlParameter { ParameterName = "@diachi", Value = user.diaChi },
                                new SqlParameter { ParameterName = "@moTa", Value = user.moTa },

                                new SqlParameter { ParameterName = "@soDienThoai", Value = user.soDienThoai },
                                new SqlParameter { ParameterName = "@email", Value = user.email },
                                new SqlParameter { ParameterName = "@loaiNguoiDung", Value = user.loaiNguoiDung },
                                erorr);
            if (erorr.Value.ToString().Trim() == "")
                return true;
            return false;
        }
        //        //Thay đổi mật khẩu
        //        public static void UpdatePass(string pass, string username)
        //        {
        //            string sql = "update NguoiDung set matKhau = '" + pass + "' where tenDangNhap = " + username;
        //            p.ExecuteNonQuery(sql, CommandType.Text);
        //        }

        //        public static void UpdateInfo(UsersDTO user)
        //        {
        //            string sql = "update users set fullname = N'" + user.fullname + "', address = N'" + user.address + "', email = '" + user.email + "', phone = '" + user.phone + "' where id = " + user.id;
        //            DataProvider.ExecuteNonQuery(sql);
        //        }
        //        public static void AddUser(UsersDTO user)
        //        {
        //            string sql = "insert into users (username, password, email, level, fullname, address, phone, created_at, updated_at, deleted) "
        //                + "values ('" + user.username + "', '" + user.password + "', '" + user.email + "', " + user.level + ", N'" + user.fullname + "', N'"
        //                + user.address + "', '" + user.phone + "', '" + user.created_at.ToString("yyyy-MM-dd HH:mm:ss") + "','" + user.updated_at.ToString("yyyy-MM-dd HH:mm:ss") + "', 'False')";
        //            int t = DataProvider.ExecuteNonQuery(sql);
        //        }
        //        public static UsersDTO FindUser(string name)
        //        {
        //            UsersDTO user = new UsersDTO();
        //            string sql = "select * from users where username = '" + name + "'";
        //            DataTable dt = DataProvider.ExecuteQuery(sql);
        //            user.id = int.Parse(dt.Rows[0][0].ToString());
        //            user.username = dt.Rows[0][1].ToString();
        //            user.password = dt.Rows[0][2].ToString();
        //            user.email = dt.Rows[0][3].ToString();
        //            user.level = int.Parse(dt.Rows[0][4].ToString());
        //            user.fullname = dt.Rows[0][5].ToString();
        //            user.address = dt.Rows[0][6].ToString();
        //            user.phone = dt.Rows[0][7].ToString();
        //            return user;
        //        }
        //        public static UsersDTO FindUser(int id)
        //        {
        //            UsersDTO user = new UsersDTO();
        //            string sql = "select * from users where id = " + id;
        //            DataTable dt = DataProvider.ExecuteQuery(sql);
        //            user.username = dt.Rows[0][1].ToString();
        //            user.password = dt.Rows[0][2].ToString();
        //            user.email = dt.Rows[0][3].ToString();
        //            user.level = int.Parse(dt.Rows[0][4].ToString());
        //            user.fullname = dt.Rows[0][5].ToString();
        //            user.address = dt.Rows[0][6].ToString();
        //            user.phone = dt.Rows[0][7].ToString();
        //            return user;
        //        }

        //        public static void UpdateUser(UsersDTO user)
        //        {
        //            string sql = "update users set "
        //                + "username = '" + user.username + "', password = '" + user.password + "', email = '" + user.email + "', level = " + user.level + ", fullname = N'" + user.fullname + "', address = N'"
        //                + user.address + "', phone = '" + user.phone + "', updated_at = '" + user.updated_at.ToString("yyyy-MM-dd HH:mm:ss") + "' where id = " + user.id;
        //            int t = DataProvider.ExecuteNonQuery(sql);
        //        }

        //        public static int IsAdmin(string username)
        //        {
        //            int level = -1;
        //            if (username != null)
        //            {
        //                string sql = "select level from users where username = '" + username.Replace("'", "''") + "'";
        //                DataTable dt = DataProvider.ExecuteQuery(sql);
        //                if (dt.Rows.Count > 0)
        //                {
        //                    level = int.Parse(dt.Rows[0][0].ToString());
        //                    return level;
        //                }
        //            }
        //            return level;
        //        }
        //        public List<UsersDTO> LayDanhSach()
        //        {
        //            string sql = "select * from users  where deleted = 'false'";
        //            DataTable dt = DataProvider.ExecuteQuery(sql);
        //            List<UsersDTO> lst = new List<UsersDTO>();
        //            foreach (DataRow dr in dt.Rows)
        //            {
        //                UsersDTO user = new UsersDTO();
        //                user.id = (int)dr["id"];
        //                user.username = dr["username"].ToString();
        //                user.password = dr["password"].ToString();
        //                user.email = dr["email"].ToString();
        //                user.level = (int)dr["level"];
        //                user.fullname = dr["fullname"].ToString();
        //                user.address = dr["address"].ToString();
        //                user.phone = dr["phone"].ToString();
        //                user.remember_token = dr["remember_token"].ToString();
        //                user.created_at = (DateTime)dr["created_at"];
        //                user.updated_at = (DateTime)dr["updated_at"];

        //                lst.Add(user);
        //            }
        //            return lst;
        //        }

        //        public UsersDTO LayThongTinTheoID(int ID)
        //        {
        //            string sql = "select * from users where deleted = 'false' and id = " + ID.ToString();
        //            DataTable dt = DataProvider.ExecuteQuery(sql);
        //            DataRow dr = dt.Rows[0];
        //            UsersDTO user = new UsersDTO();
        //            user.id = (int)dr["id"];
        //            user.username = dr["username"].ToString();
        //            user.password = dr["password"].ToString();
        //            user.email = dr["email"].ToString();
        //            user.level = (int)dr["level"];
        //            user.fullname = dr["fullname"].ToString();
        //            user.address = dr["address"].ToString();
        //            user.phone = dr["phone"].ToString();
        //            user.remember_token = dr["remember_token"].ToString();
        //            user.created_at = (DateTime)dr["created_at"];
        //            user.updated_at = (DateTime)dr["updated_at"];

        //            return user;
        //        }

        //        public void Them(UsersDTO user)
        //        {
        //            List<string> pname = new List<string>();
        //            ArrayList values = new ArrayList();
        //            pname.Add("@id");
        //            pname.Add("@username");
        //            pname.Add("@password");
        //            pname.Add("@email");
        //            pname.Add("@level");
        //            pname.Add("@fullname");
        //            pname.Add("@address");
        //            pname.Add("@phone");
        //            pname.Add("@remember_token");
        //            pname.Add("@created_at");

        //            values.Add(user.id);
        //            values.Add(user.username);
        //            values.Add(user.password);
        //            values.Add(user.email);
        //            values.Add(user.level);
        //            values.Add(user.fullname);
        //            values.Add(user.address);
        //            values.Add(user.phone);
        //            values.Add(user.remember_token);
        //            values.Add(user.created_at.Date.ToString());

        //            DataProvider.ExecuteStoreProc("usp_InsertUser", pname, values);
        //        }
    }
}