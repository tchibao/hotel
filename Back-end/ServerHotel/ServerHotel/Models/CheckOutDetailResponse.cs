﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerHotel.Models
{
    public class CheckOutDetailResponse
    {
        public string[] phongNumber { get; set; }
        public int TotalRoom { get; set; }
        public int totalMoney { get; set; }
    }
}