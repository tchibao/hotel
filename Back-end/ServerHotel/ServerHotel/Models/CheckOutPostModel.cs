﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerHotel.Models
{
    public class CheckOutPostModel
    {
        public string maDP { get; set; }
        public string[] lstPhong { get; set; }
        public int tongTien { get; set; }
    }
}