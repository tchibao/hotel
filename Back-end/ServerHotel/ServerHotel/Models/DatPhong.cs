﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerHotel.Models
{
    public class DatPhong
    {
        public string maDP {get; set;}
        public string maLoaiPhong {get; set;}
        public string maKH { get; set;}
        public DateTime ngayBatDau { get; set; }
        public DateTime ngayTraPhong { get; set; }
        public DateTime ngayDat { get; set; }
        public int donGia { get; set; }
        public string moTa { get; set; }
        public int tinhTrang { get; set; }
        public int slDat { get; set; }
    }
}