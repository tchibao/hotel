﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ServerHotel.Models
{
    public class DatPhongModel
    {
        public string maDB { get; set; }

        [Required(ErrorMessage = "Mã loại phòng chưa nhập")]
        public string maLoaiPhong { get; set; }
        [Required(ErrorMessage = "Mã khách hàng chưa nhập")]
        public string maKH { get; set; }
        [Required(ErrorMessage = "Ngày bắt đầu chưa nhập")]
        public DateTime ngayBatDau { get; set; }
        [Required(ErrorMessage = "Ngày trả phòng chưa nhập")]
        public DateTime ngayTraPhong { get; set; }
        public DateTime ngayDat { get; set; }
        [Required(ErrorMessage = "Ngày trả phòng chưa nhập")]
        public int DonGia { get; set; }
        public string moTa { get; set; }
        public int tinhTrang { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Số lượng đặt không hợp lệ")]
        public int slDat { get; set; }


    }
}