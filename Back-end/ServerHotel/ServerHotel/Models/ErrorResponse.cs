﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerHotel.Models
{
    public class ErrorResponse
    {
        public ErrorResponse()
        {
            errors = new List<string>();
        }
        public List<string> errors { get; set; }
    }
}