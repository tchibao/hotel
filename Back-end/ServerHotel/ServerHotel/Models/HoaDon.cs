﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerHotel.Models
{
    public class HoaDon
    {
        public string maHD { get; set; }
        public DateTime ngayThanhToan { get; set; }
        public int tongTien { get; set; }
        public string maDP { get; set; }
    }
}