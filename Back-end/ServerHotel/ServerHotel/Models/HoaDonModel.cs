﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerHotel.Models
{
    public class HoaDonModel
    {
        public string maHD { get; set; }
        public DateTime ngayThanhToan { get; set; }
        public int TongTien { get; set; }
        public string maDP { get; set; }

    }
}