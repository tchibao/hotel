﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerHotel.Models
{
    public class Hotel
    {
        public string maKS {get; set;}
        public string tenKS { get; set; }
        public float soSao { get; set; }
        public string soNha { get; set; }
        public string duong { get; set; }
        public string quan { get; set; }
        public string thanhPho { get; set; }
        public int giaTB { get; set; }
        public string moTa { get; set; }
        public string avatar { get; set; }


        //
        public string avatarID { get; set; }
    }
}