﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerHotel.Models
{
    public class LoaiPhong
    {
        public string maLoaiPhong { get; set; }
        public string tenLoaiPhong { get; set; }
        public string maKS { get; set; }
        public int donGia { get; set; }
        public string moTa { get; set; }
        public int slTrong { get; set; }
        public string testImg { get; set; }

        public string detailImage1 { get; set; }
        public string detailImage2 { get; set; }
        public string detailImage3 { get; set; }
        public string detailImage4 { get; set; }

    }
}