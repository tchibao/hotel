﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ServerHotel.Models
{
    public class Phong
    {
        [Required]
        public string maPhong { get; set; }
        [Required]
        public string loaiPhong { get; set; }
        public int daDat { get; set; }
        public bool isDelete { get; set; }
    }
}