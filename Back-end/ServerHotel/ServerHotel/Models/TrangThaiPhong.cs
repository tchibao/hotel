﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServerHotel.Models
{
    public class TrangThaiPhong
    {
        public string maPhong { get; set; }
        public DateTime ngay { get; set;}
        public int tinhTrang { get; set; }
        public string maDP { get; set; }
    }
}