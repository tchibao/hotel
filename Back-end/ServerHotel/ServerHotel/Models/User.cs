﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ServerHotel.Models
{
    public class User
    {
        [Required]
        public string maNguoiDung { get; set; }
        [Required]
        public string hoTen { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z0-9]+(?:[_ -]?[a - zA - Z0 - 9])*$", ErrorMessage = "Tên đăng nhập không được chứa ký tự đặc biệt")]
        public string tenDangNhap { get; set; }

       
        public string matKhau { get; set; }
        [Required]
        public string soCMND { get; set; }

        public string diaChi { get; set; }
        [Required]
        [RegularExpression(@"^(01[2689]|09)[0-9]{8}$", ErrorMessage = "Số điện thoại không hợp lệ")]
        public string soDienThoai { get; set; }


        public string moTa { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Not a valid email")]
        public string email { get; set; }

        public bool isDelete { get; set; }

        public int loaiNguoiDung { get; set; }
    }
}