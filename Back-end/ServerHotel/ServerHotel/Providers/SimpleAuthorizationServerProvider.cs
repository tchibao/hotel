﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using ServerHotel.BUS;
using ServerHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace ServerHotel.Providers
{
    public class SimpleAuthorizationServerProvider: OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            //Check password
            User user = Auth.checkLogin(context.UserName, context.Password);
            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect");
                return;
            }
            //if (context.UserName != context.Password)
            //{
            //    context.SetError("invalid_grant", "The user name or password is incorrect");
            //    return;
            //}
            //var user = Auth.GetInfoUser(context.UserName);
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            //identity.AddClaim(new Claim("sub", context.UserName));
            //identity.AddClaim(new Claim("role", user.loaiNguoiDung.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.Role , user.loaiNguoiDung.ToString()));
            var props = new AuthenticationProperties(new Dictionary<string, string>
            {
                {
                    "username", user.tenDangNhap
                },
                {
                    "id", user.maNguoiDung
                },
                {
                    "loaiNguoiDung", user.loaiNguoiDung.ToString()
                }
            });
            //context.Validated(identity);
            var ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);

        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}