﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ServerHotel.Ults
{
    public class CloudiaryServices
    {
        
        private static CloudiaryServices instance;
        public static CloudiaryServices Singleton
        {
            get
            {
                if(instance == null)
                {
                    instance = new CloudiaryServices();
                   // SetUPCloudinaryService();
                }
                return instance;
            }
        }

        private Cloudinary _cloudinary;

        public CloudiaryServices()
        {
            SetUPCloudinaryService();
        }

        public void DeleteResource(string id)
        {

            var delParams = new DelResParams()
            {
                PublicIds = new List<string>() { id },
                Invalidate = true
            };
            _cloudinary.DeleteResources(delParams);
        }


        public Task<ImageUploadResult> UploadImage(ImageUploadParams param)
        {
           return _cloudinary.UploadAsync(param);
        }
        private  void SetUPCloudinaryService(string apiKey = "998173262265883", string apiSecret = "elfggyRECMbSU8Uihp-ICeZnWtU", string cloudName = "doga4u6vf")
        {
            var myAccount = new Account { ApiKey = apiKey, ApiSecret = apiSecret, Cloud = cloudName };
            _cloudinary = new Cloudinary(myAccount);
        }
    }
}