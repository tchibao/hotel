﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;

namespace ServerHotel.Ults
{
    public class Helper
    {
        public static string Encrypt(string password)
        {
        
            var provider = new SHA1CryptoServiceProvider();
            var encoding = new UnicodeEncoding();
            byte[] encrypted =  provider.ComputeHash(encoding.GetBytes(password));

            //return as base64 string
            return Convert.ToBase64String(encrypted);
        }

    }
}