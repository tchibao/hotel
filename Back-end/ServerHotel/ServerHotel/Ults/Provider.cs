﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ServerHotel.Ults
{
    public class Provider
    {
        //public static string ConnectionString = @"Server=localhost; Database= QLKhoCauHoi;Trusted_Connection=True;User Id= sa; Password=1;";
        public static string ConnectionString = @"Server=.; Database= QLKhachSan;Trusted_Connection=True;";
        //public static string ConnectionString = @"Server=.; Database= QLKhachSan;Trusted_Connection=True;";


        //public static string ConnectionString = @"Server=X1VGGHEKSPVH7CP\SQLEXPRESS; Database= QLKhoCauHoi;Trusted_Connection=True;";

        SqlConnection connection;

        public SqlConnection Connection
        {
            get { return connection; }
            set { connection = value; }
        }
        public DataTable Select(string strSql, CommandType cmdType, params SqlParameter[] parameters)
        {
            Connect();
            try
            {
                SqlCommand command = Connection.CreateCommand();
                command.CommandText = strSql;
                command.CommandType = cmdType;
                if (parameters != null && parameters.Length > 0)
                {
                    command.Parameters.AddRange(parameters);
                }
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataTable ds = new DataTable();
                da.Fill(ds);
                Disconnect();
                return ds;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Start: Open connect
        /// </summary>
        /// 




        public void Connect()
        {
            try
            {
                if (Connection == null)
                    Connection = new SqlConnection(ConnectionString);
                if (Connection.State != ConnectionState.Closed)
                    Connection.Close();
                Connection.Open();
            }


            catch (SqlException ex)
            {
                throw ex;
            }
        }
        public void Disconnect()
        {
            if (Connection != null && Connection.State == ConnectionState.Open)
                Connection.Close();
        }

        /// <summary>
        /// Insert, Update, Delete
        /// </summary>
        /// <returns>number row effects</returns>
        public int ExecuteNonQuery(string strSql, CommandType cmdType, params SqlParameter[] parameters)
        {
            Connect();
            try
            {
                SqlCommand command = Connection.CreateCommand();
                command.CommandText = strSql;
                command.CommandType = cmdType;

                if (parameters != null && parameters.Length > 0)
                    command.Parameters.AddRange(parameters);

                int nRow = command.ExecuteNonQuery();
                Disconnect();
                return nRow;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public object ExecuteNonQueryReturn(string strSql, CommandType cmdType, params SqlParameter[] parameters)
        {
            Connect();
            try
            {
                SqlCommand command = Connection.CreateCommand();
                command.CommandText = strSql;
                command.CommandType = cmdType;

                if (parameters != null && parameters.Length > 0)
                    command.Parameters.AddRange(parameters);

                SqlParameter returnParameter = command.Parameters.Add("out", SqlDbType.NVarChar, 200);
                returnParameter.Direction = ParameterDirection.ReturnValue;
                int nRow = command.ExecuteNonQuery();
                object returnvalue = returnParameter.Value;
                Disconnect();
                return returnvalue;

            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
    }
}