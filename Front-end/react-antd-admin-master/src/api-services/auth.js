export const postLogin = (username, password) => ({
    method: 'POST',
    url: '/token',
    data: "userName=" + username + "&password=" + password+ 
    "&grant_type=password"
  });
  
  
  export const postSignUp = (tenDangNhap, matKhau,hoTen,soCMND,soDienThoai,email,repassword,moTa,diaChi) => ({
    method: 'POST',
    url: 'api/user',
    data: {
      tenDangNhap, 
      matKhau,
      hoTen,
      soCMND,
      diaChi,
      soDienThoai,
      moTa,
      email,
      loaiNguoiDung: 1
    }
  });
  