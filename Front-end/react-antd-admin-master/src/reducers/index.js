import { combineReducers } from 'redux';
import authReducer from './auth';
import menu from './menu';

const rootReducer = combineReducers({
  authReducer,
  menu,
});

export default rootReducer;
